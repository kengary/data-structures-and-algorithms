/*
 * author		: [HOW WEI KENG]
 * matric no.	: [A0087836]
 */
class MagicBoxFiller {

    int N, caseChecker, curRow, curCol, totalNum;
    int[][] magicBoxArr;

    public MagicBoxFiller(int n) {
        this.N = n;
        this.totalNum = (int) Math.pow(N, 2);
    }

    public void runSolution() {
        this.magicBoxArr = new int[N][N];
        if (N == 1) {
            System.out.println(1);
            System.exit(0);
        } else {
            for (int i = 1; i <= this.totalNum; i++) {
                if (i != 1) {

                    if (this.curRow == 0 && this.curCol != (N - 1)) {
                        caseChecker = 1;//first row but not right most col

                    } else if (this.curRow == 0 && this.curCol == (N - 1)) {
                        caseChecker = 3;
                        //Action at topmost rightmost col
                    } else if (this.curCol == (N - 1)) {
                        caseChecker = 2;
//Action at the rightmost col
                    } else {
                        caseChecker = 4;

                    }

                    checkCase(i, caseChecker);

                } else {
                    this.curRow = 0;
                    this.curCol = ((N / 2));
                    this.magicBoxArr[curRow][curCol] = i;

                }

            }

        }

    }

    private void checkCase(int i, int c) {
        int rowChecker = 0, colChecker = 0;
        switch (c) {
            case 1:
                //Action at the first row but not right most col
                colChecker = this.curCol + 1;
                rowChecker = N - 1;
                if ((checkEmpty(i, rowChecker, colChecker))) {
                    this.curRow = N - 1;
                    this.curCol += 1;
                    this.magicBoxArr[this.curRow][this.curCol] = i;
                }
                break;

            case 2:
                //Action at the rightmost col
                colChecker = 0;
                rowChecker = this.curRow - 1;
                if ((checkEmpty(i, rowChecker, colChecker)) == true) {
                    this.curRow -= 1;
                    this.curCol = 0;
                    this.magicBoxArr[this.curRow][this.curCol] = i;
                }
                break;

            case 3:
                //Action at topmost rightmost col
                this.curRow += 1;
                this.magicBoxArr[this.curRow][this.curCol] = i;
                break;

            default:
                colChecker = this.curCol + 1;
                ;
                rowChecker = this.curRow - 1;
                if ((checkEmpty(i, rowChecker, colChecker)) == true) {
                    this.curRow -= 1;
                    this.curCol += 1;
                    this.magicBoxArr[this.curRow][this.curCol] = i;
                }
                break;

        }
    }

    private boolean checkEmpty(int i, int cr, int cl) {
        boolean c = true;

        if (this.magicBoxArr[cr][cl] != 0) {
            this.curRow += 1;
            this.magicBoxArr[this.curRow][this.curCol] = i;
            c = false;
        } else {
            c = true;
        }

        return c;
    }

    public void printResult() {
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                    System.out.print(this.magicBoxArr[i][j]);
                    if(j!=(N-1)){
                        System.out.print(" ");
                        }else{
                            System.out.print("\n");
                            }
            }
        }

    }
}
