/*
 * author		: [How Wei Keng]
 * matric no.	: [A0087836]
 */
// create an object called Person that has attribute name, height, weight
public class Person {
	// declare the attributes
	String name;
double height,weight;
	// declare the constructor
public Person(String name,double height,double weight){
this.name=name;
this.height=height;
this.weight=weight;

}
	/* use this method to compute the BMI for that person
	 * 		PRE-Condition  : 
	 * 		POST-Condition :
	 */
	public double computeBMI() {
		// implement the BMI formula
double BMI;
BMI=this.weight/(Math.pow((this.height/100.0),2));
return BMI;
	}
}