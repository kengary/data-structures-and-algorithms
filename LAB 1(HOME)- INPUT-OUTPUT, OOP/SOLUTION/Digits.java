import java.util.*;
/*
 * author		: [How Wei Keng]
 * matric no.	: [A0087836M]
 */

import java.util.*;

public class Digits {
    /* use this method to sum the digits of a number called x
     * For example, x = 13, it should return 4 as 1 + 3 = 4
     * 		PRE-Condition  :
     * 		POST-Condition :
     */

    public static int sumOfDigits(int x) {
        // process the sum of digits of x
        int sum = 0;
        String y = "" + x;
        for (int i = 0; i < y.length(); i++) {
            sum += Integer.parseInt(y.substring(i, i + 1));
        }
        return sum;
    }

    public static void main(String[] args) {
        // declare the necessary variables
        int N, K, output = 0;

        // declare a Scanner object to read input
        Scanner scan = new Scanner(System.in);

        // read input and process them accordingly
        boolean errChecker = false;

        try {
            N = scan.nextInt();
            K = scan.nextInt();
            if (N < 1 || N > 100000 || K < 1 || K > 50) {
                System.exit(0);
            } else {
                for (int i = 0; i <= N; i++) {
                    if (sumOfDigits(i) == K) {
                        output++;
                    }
                }
            }

        } catch (Exception ex) {
            System.exit(0);
        }


        // simulate the problem

        // output the result
        System.out.println(output);
    }
}