/*
 * author		: [How Wei Keng]
 * matric no.	: [A0087836]
 */

import java.util.*;

// create an object called Person that has attribute name, height, weight
public class Person {
	// declare the attributes
	String name;
double height,weight;
	// declare the constructor
public Person(String name,double height,double weight){
this.name=name;
this.height=height;
this.weight=weight;

}
	/* use this method to compute the BMI for that person
	 * 		PRE-Condition  : 
	 * 		POST-Condition :
	 */
	public double computeBMI() {
		// implement the BMI formula
double BMI;
BMI=this.weight/(Math.pow((this.height/100.0),2));
return BMI;
	}
}

public class Measurement {

    public static void main(String[] args) {
        // declare the necessary variables
        double shortestHeight = 1000, shortestWeight=0, tallestHeight = 0, tallestWeight=0;
        String shortestName = "", tallestName = "";
        int numOfPplN;
        double height, weight;
        String name;
        boolean errChecker = false;

        // declare a Scanner object to read input
        Scanner scan = new Scanner(System.in);

        // read input and process them accordingly
        do {
            try {
                numOfPplN = scan.nextInt();
                if (numOfPplN < 1 || numOfPplN > 100) {
                    System.exit(0);
                } else {
                    for (int i = 0; i < numOfPplN; i++) {
                        name = scan.next();
                        height = scan.nextInt();
                        weight = scan.nextInt();

                        if (height < shortestHeight) {
                            shortestHeight = height;
                            shortestWeight = weight;
                            shortestName = name;
                        }
                        if (height > tallestHeight) {
                            tallestHeight = height;
                            tallestWeight = weight;
                            tallestName = name;
                        }
                    }
                }

                }catch  (Exception ex) {
                errChecker = true;
            }

        } while (errChecker == true);

        // simulate the problem
        Person shortPerson = new Person(shortestName, shortestHeight, shortestWeight);
        Person tallPerson = new Person(tallestName, tallestHeight, tallestWeight);
        // output the result
        System.out.printf(shortPerson.name + " is the shortest with BMI equals to %.2f.%n", shortPerson.computeBMI());
        System.out.printf(tallPerson.name + " is the tallest with BMI equals to %.2f.%n", tallPerson.computeBMI());
    }
}