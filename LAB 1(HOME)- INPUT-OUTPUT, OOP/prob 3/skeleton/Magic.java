/*
 * author		: [HOW WEI KENG]
 * matric no.	: [A0087836]
 */

import java.util.*;

public class Magic {
	
	public static void main(String[] args) {		
		// declare the necessary variables
		int N;
		MagicBoxFiller m1;

		// declare a Scanner object to read input
		Scanner scan=new Scanner(System.in);

		// read input and process them accordingly
		N=scan.nextInt();

		// implement the algorithm
m1=new MagicBoxFiller(N);
m1.runSolution();

		// output the result
m1.printResult();

	}
}