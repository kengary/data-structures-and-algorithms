/**
Name: How Wei Keng
Matri.: A0087836M
 **/

import java.util.*;

// use ListNode to represent a person in the circle
class ListNode<Integer> {
    private int _prince;
    private ListNode<Integer> next;
    private ListNode<Integer> prev;

    public ListNode(int prince){
        _prince=prince;
        this.next=null;
        this.prev=null;
    }

    public int getPrince(){
        return _prince;
    }
    public ListNode<Integer> getNext(){
        return next;
    }
    public ListNode<Integer> getPrev(){
        return prev;
    }
    public void setNext(ListNode<Integer> newNext){
        this.next=newNext;
    }
    public void setPrev(ListNode<Integer> newPrev){
        this.prev=newPrev;
    }    

}

class Result {

    // declare the member field
    int num_nodes;
    ListNode<Integer> head=null,tail=null,current=null;    

    // declare the constructor
    public Result(int N){
        num_nodes=0;
        initList(N);
    }

    public void initList(int N){
        for(int i=1;i<=N;i++){
            add(new ListNode<Integer>(i));
        }
        tail=current;
        tail.setNext(head);
        head.setPrev(tail);
        current=head;
    }
    public void add(ListNode<Integer> newNode){
        if(num_nodes==0){
            head=newNode;
            current=head;
        }else{
            current.setNext(newNode);
            newNode.setPrev(current);
            current=newNode;
        }      
        num_nodes++;
    }    
    /* 
       removing K-th person and update the state
     * 		PRE-Condition  :
     * 		POST-Condition :
     */
    private void remove(ListNode<Integer> oldCurrent) {
        // implementation
        ListNode<Integer> tempNode=oldCurrent;

        tempNode.getNext().setPrev(current.getPrev());
        tempNode.getPrev().setNext(current.getNext());

        current=current.getNext();

        tempNode.setNext(null);
        tempNode.setPrev(null);

        num_nodes--;
    } 

    /* keep removing K-th person from the circle
     * 		PRE-Condition  :
     * 		POST-Condition :
     */
    public void solve(int K) {
        // implementation
        while(num_nodes!=1){
            for(int i=0;i<(K-1);i++){
                current=current.getNext();
            }
            System.out.print(current.getPrince()+" ");
            remove(current);
        }
        System.out.print(current.getPrince()+" ");
        System.out.println();
    }
}

public class Josephine {

    public static void main(String[] args) {

        // declare the necessary variables
        int T,N,K;

        Scanner scan=new Scanner(System.in);

        T=scan.nextInt();

        for(int i=0;i<T;i++){
            N=scan.nextInt();
            Result rs=new Result(N);
            K=scan.nextInt();
            rs.solve(K);
        }
    }
}
