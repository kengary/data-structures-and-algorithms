import java.util.*;

// use ListNode to represent a person in the circle
class ListNode {

}

class Result {
	
    // declare the member field

    // declare the constructor

	/* removing K-th person and update the state
	 * 		PRE-Condition  :
	 * 		POST-Condition :
	 */
	private void remove(int K) {
		// implementation
	}

	/* keep removing K-th person from the circle
	 * 		PRE-Condition  :
	 * 		POST-Condition :
	 */
	public void solve() {
		// implementation
	}
}

public class Josephine {
	
	public static void main(String[] args) {
        
		// declare the necessary variables

		// create new object from class Result

		// declare a Scanner object to read input

		// read input and process them accordingly
	}
}