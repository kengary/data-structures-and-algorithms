import java.util.*;
/*
 * author		: [How Wei Keng]
 * matric no.	: [A0087836M]
 */

// use ListNode to represent the balls.
class ListNode <E> {

    //mbr field
    private E element;
    private ListNode <E> next;
    private ListNode <E> prev;

    //Constructor
    public ListNode(E item){
        this.element=item;
        this.next=null;
        this.prev=null;
    }

    //getters
    public E getElement(){
        return this.element;
    }

    public ListNode<E> getNext(){
        return this.next;
    }

    public ListNode<E> getPrev(){
        return this.prev;
    }

    //setters
    public void setElement(E item){
        this.element=item;
    }

    public void setNext(ListNode<E> newNext){
        this.next=newNext;
    }

    public void setPrev(ListNode<E> newPrev){
        this.prev=newPrev;
    }

}

class Result {

    // declare the member field
    private ListNode <Integer> head;
    private ListNode <Integer> tail;
    private int size;

    // declare the constructor
    Result(int N){
        head=new ListNode<Integer>(null);
        tail=new ListNode<Integer>(null);
        head.setNext(tail);
        tail.setPrev(head);
        initBalls(N);	
    }

    //getters
    /* implement the operation for getNode
     * 		PRE-Condition  : item!=null
     * 		POST-Condition :return the node that contains the item
     */
    private ListNode <Integer> getNode(int item){
        ListNode<Integer> tmpNode= head;
        ListNode<Integer> tmpNode2= tail;

        for(int i=0;i<size;i++){
            tmpNode=tmpNode.getNext();			
            tmpNode2=tmpNode2.getPrev();			
            if(tmpNode.getElement()==item){
                break;
            }
            if(tmpNode2.getElement()==item){
                tmpNode=tmpNode2;
                break;
            }
            if(tmpNode==tmpNode2){
                System.exit(0);	
            }		
        }
        return tmpNode;
    }

    /* implement the operation for remove
     * 		PRE-Condition  :given node!= null
     * 		POST-Condition :remove the given node
     */
    public void remove(ListNode<Integer> given_node){
        given_node.getPrev().setNext(given_node.getNext());
        given_node.getNext().setPrev(given_node.getPrev());
        given_node.setPrev(null);
        given_node.setNext(null);		
        size--;
    }

    /* implement the operation for addBefore
     * 		PRE-Condition  :this node!=null, x!=null
     * 		POST-Condition :create new node and add it before this node
     */
    public void addBefore(ListNode<Integer> this_node,int x){
        ListNode <Integer> newNode=new ListNode<Integer>(x);
        newNode.setPrev(this_node.getPrev());
        newNode.setNext(this_node);
        this_node.getPrev().setNext(newNode);
        this_node.setPrev(newNode);
        size++;

    }

    /* implement the operation for A
     * 		PRE-Condition  :x,y != null
     * 		POST-Condition :remove node that contains y,create new y node infront x
     */	
    public void doA(int x, int y) {
        // implementation		
        remove(getNode(x));
        addBefore(getNode(y),x);		
    }

    /* implement the operation for B
     * 		PRE-Condition  :x,y != null
     * 		POST-Condition :remove node that contains x,create new x node behind y
     */
    public void doB(int x, int y) {
        // implementation
        remove(getNode(x));
        addBefore(getNode(y).getNext(),x);

    }
    /* implement the operation for R
     * 		PRE-Condition  :x!=null
     * 		POST-Condition :remove the ball x
     */
    public void doR(int x) {
        // implementation
        remove(getNode(x));
    }
    /* implement the operation for R
     * 		PRE-Condition  :x!=null
     * 		POST-Condition :create the ball item infront of tail
     */
    private void initBalls(int x) {
        // implementation
        for(int i=1;i<(x+1);i++){
            addBefore(tail,i);					
        }
    }

    /* implement the operation for print
     * 		PRE-Condition  :-
     * 		POST-Condition : print all the balls in pos.
     */
    public void print(){
        ListNode<Integer> tmpNode=head;
        for(int i=0;i<size;i++){
            tmpNode=tmpNode.getNext();
            System.out.print(tmpNode.getElement());
            System.out.print(" ");
        }
        System.out.println();
    }

}

//driver class
public class Balls {

    public static void main(String[] args) {

        // declare the necessary variables		
        int N=0,M=0;

        // declare a Scanner object to read input
        Scanner scan=new Scanner(System.in);

        // read input and process them accordingly
        // create new object from class Result
        N=scan.nextInt();

        Result r=new Result(N);
        M=scan.nextInt();

        for(int i=0;i<M;i++){
            String opStr=scan.next();
            if(opStr.equals("A")){
                r.doA(scan.nextInt(),scan.nextInt());
            }
            else if(opStr.equals("B")){
                r.doB(scan.nextInt(),scan.nextInt());
            }
            else if(opStr.equals("R")){
                r.doR(scan.nextInt());
            }
            else{
                System.exit(0);
            }
        }
        //print result
        r.print();
    }
}
