import java.util.*;

class BetterList<E>{
    // declare the member field
LinkedList<Integer> ls;
int _K;
    // declare the constructor
public BetterList(int k){
ls=new LinkedList<Integer>();
_K=k;
}
	/* add: add a listNode to the linklist
	 * 		PRE-Condition  :
	 * 		POST-Condition :
	 */
	 
	public LinkedList<Integer> getOldList() {
		// implementation
		return ls;
	} 
	public void add(int n) {
		// implementation
		ls.add(n);
	}

	/* insert: insert a newInteger at index
	 * 		PRE-Condition  :
	 * 		POST-Condition :
	 */
	public void insert(int index, int newInteger) {
		// implementation

		ls.add((index-1),newInteger);

	}


	/* remove: remove the element at index
	 * 		PRE-Condition  :
	 * 		POST-Condition :
	 */
	public void remove(int index) {
		// implementation
		ls.remove((index-1));
	}


	/* change: change the integer at index with newInteger
	 * 		PRE-Condition  :
	 * 		POST-Condition :
	 */
	public void change(int index, int newInteger) {
		// implementation
		ls.set((index-1),newInteger);
	}


	/* isBetter: to compare between this linkedList with prevLinkedList
	 * 		PRE-Condition  :
	 * 		POST-Condition :
	 */
	public String isBetter(LinkedList<Integer> previousList) {
		// implementation		
		int sumOfDiff=0;
		int size=(previousList.size()<ls.size()?previousList.size():ls.size());
		for(int i=0;i<size;i++){
		sumOfDiff+=Math.abs(previousList.get(i)-ls.get(i));		
		}
		if(sumOfDiff<=_K)
		return "NO";

		return "YES";
	}
}

public class Main {
	
	public static void main(String[] args) {
        
		System.out.println(true!);
		
		
		// declare the necessary variables
		int N,K,numOfOps;

		// create new object from class Result
		// declare a Scanner object to read input
		
		Scanner scan=new Scanner(System.in);
		N=scan.nextInt();
		K=scan.nextInt();
		BetterList <Integer> ls=new BetterList<Integer>(K);
		
		// read input and process them accordingly
		for(int i=0;i<N;i++){
		ls.add(scan.nextInt());
		}
		
		numOfOps=scan.nextInt();
		String opStr;
		for(int j=0;j<numOfOps;j++){
		
		LinkedList<Integer> temp=new LinkedList<Integer>();
		
		for(int i:ls.getOldList()){
		temp.add(i);
		}
		opStr=scan.next();
		if(opStr.equals("R"))
		ls.remove(scan.nextInt());
		else if(opStr.equals("I"))
		ls.insert(scan.nextInt(),scan.nextInt());
		else
		ls.change(scan.nextInt(),scan.nextInt());
		System.out.println(ls.isBetter(temp));
		}
	}
}