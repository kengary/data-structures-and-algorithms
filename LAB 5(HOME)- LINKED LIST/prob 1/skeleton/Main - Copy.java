import java.util.*;

//use ListNode to represent the integers.
class ListNode {

}

class LinkedList {
    // declare the member field

    // declare the constructor

	/* add: add a listNode to the linklist
	 * 		PRE-Condition  :
	 * 		POST-Condition :
	 */
	public void add(ListNode listNode) {
		// implementation
	}

	/* insert: insert a newInteger at index
	 * 		PRE-Condition  :
	 * 		POST-Condition :
	 */
	public void insert(int index, int newInteger) {
		// implementation
	}


	/* remove: remove the element at index
	 * 		PRE-Condition  :
	 * 		POST-Condition :
	 */
	public void remove(int index) {
		// implementation
	}


	/* change: change the integer at index with newInteger
	 * 		PRE-Condition  :
	 * 		POST-Condition :
	 */
	public void change(int index, int newInteger) {
		// implementation
	}


	/* isBetter: to compare between this linkedList with prevLinkedList
	 * 		PRE-Condition  :
	 * 		POST-Condition :
	 */
	public String isBetter(LinkedList prevLinkedList) {
		// implementation
		return "YES";
	}
}

public class Main {
	
	public static void main(String[] args) {
        
		// declare the necessary variables

		// create new object from class Result

		// declare a Scanner object to read input

		// read input and process them accordingly
	}
}