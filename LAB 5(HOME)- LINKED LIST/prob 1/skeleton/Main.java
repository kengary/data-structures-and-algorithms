import java.util.*;

/*
 * author		: [How Wei Keng]
 * matric no.	: [A0087836M]
 */

//use ListNode to represent the integers.
class ListNode<E> {

    private E element;
    private ListNode<E> next;
    private ListNode<E> prev;

    public ListNode(E item){
        this.element=item;
        this.next=null;
        this.prev=null;
    }

    //getters
    public E getElement(){
        return this.element;
    }

    public ListNode<E> getNext(){
        return this.next;
    }

    public ListNode<E> getPrev(){
        return this.prev;
    }

    //setters
    public void setElement(E item){
        this.element=item;
    }

    public void setNext(ListNode<E> newNext){
        this.next=newNext;
    }

    public void setPrev(ListNode<E> newPrev){
        this.prev=newPrev;
    }
}

class LinkedList{
    // declare the member field
    private ListNode<Integer> head;
    private ListNode<Integer> tail;
    private int num_nodes;

    // declare the constructor
    public LinkedList(){
        head=new ListNode<Integer>(null);
        tail=new ListNode<Integer>(null);
        head.setNext(tail);
        tail.setPrev(head);
        num_nodes=0;
    }

    //getters
    private int getSize(){
        return this.num_nodes;
    }

    /* getNode: search and return a node at the specific index
     * 		PRE-Condition  :index > -1
     * 		POST-Condition :returns the specific node
     */
    private ListNode<Integer> getNode(int index){
        ListNode<Integer> tmpNode;	

        if(index<num_nodes/2){
            tmpNode=head.getNext();
            for(int i=0;i<index;i++){
                tmpNode=tmpNode.getNext();
            }
        }else{
            tmpNode=tail.getPrev();
            for(int x=num_nodes-1;x>index;x--){
                tmpNode=tmpNode.getPrev();
            }
        }		
        return tmpNode;
    }

    /* addBefore: adds a new listNode obj before the specific node
     * 		PRE-Condition  :-
     * 		POST-Condition :new node inserted num of nodes+1;
     */
    private void addBefore(ListNode<Integer> given_node,int newInteger){	
        ListNode<Integer> newNode= new ListNode<Integer>(newInteger);	

        newNode.setPrev(given_node.getPrev());
        newNode.setNext(given_node);
        given_node.getPrev().setNext(newNode);
        given_node.setPrev(newNode);
        num_nodes++;
    }

    /* removeNode: removes a listNode at the specific node
     * 		PRE-Condition  :-
     * 		POST-Condition :node removed, num of nodes-1;
     */
    private void removeNode(ListNode<Integer> given_node){	

        given_node.getPrev().setNext(given_node.getNext());
        given_node.getNext().setPrev(given_node.getPrev());
        given_node.setPrev(null);
        given_node.setNext(null);
        num_nodes--;
    }

    /* add: adds a new listNode to the linklist
     * 		PRE-Condition  :newInteger must not be null
     * 		POST-Condition :add listNode before tail node
     */
    public void add(int newInteger) {
        addBefore(tail,newInteger);
    }

    /* insert: insert a newInteger at index
     * 		PRE-Condition  :index>-1,newInteger not null
     * 		POST-Condition :invokes addBefore
     */
    public void insert(int index, int newInteger) {
        // implementation		
        if((index-1)==0){
            if(getSize()==0){
                add(newInteger);
            }else
                addBefore(getNode(0),newInteger);
        }else{
            addBefore(getNode(index-1),newInteger);
        }
    }

    /* remove: remove the element at index
     * 		PRE-Condition  :index >-1
     * 		POST-Condition :invoke removeNode
     */
    public void remove(int index) {
        // implementation
        removeNode(getNode(index-1));			
    }

    /* change: change the integer at index with newInteger
     * 		PRE-Condition  :index>-1, newInteger not null
     * 		POST-Condition :invoke setElement from node
     */
    public void change(int index, int newInteger) {
        // implementation
        getNode(index-1).setElement(newInteger);			
    }

    /* copy: copy the another list to this list
     * 		PRE-Condition  :prev list must not be empty
     * 		POST-Condition :add all elements from prev to this list
     */
    public void copy(LinkedList ls){
        this.head.setNext(tail);
        this.tail.setPrev(head);
        this.num_nodes=0;
        for(int i=1;i<1+ls.getSize();i++){
            this.add(ls.getNode(i-1).getElement());

        }
    }

    /* isBetter: to compare between this linkedList with prevLinkedList
     * 		PRE-Condition  :prevList not null, K not null
     * 		POST-Condition :return Yes if sum>K, vice.versa
     */
    public String isBetter(LinkedList prevList,int K) {
        // implementation
        int sum=0;

        for(int i=1;i<1+((prevList.getSize()<this.getSize())?prevList.getSize():this.getSize());i++){

            sum+=Math.abs(prevList.getNode(i-1).getElement()-this.getNode(i-1).getElement());		
        }		

        if(sum<=K){		
            return "NO";
        }
        return "YES";
    }
}

//driver class
public class Main {

    public static void main(String[] args) {

        // declare,init the necessary variables
        int N=0,K=0;
        int item=0;
        int num_op=0;

        //Scanner
        Scanner scan=new Scanner(System.in);

        LinkedList ls=new LinkedList();

        N=scan.nextInt();
        K=scan.nextInt();

        // add int to the linkedlist
        for(int i=0;i<N;i++){
            item=scan.nextInt();
            ls.add(item);
        }

        num_op=scan.nextInt();

        // Store previous LinkedList
        LinkedList prevList=new LinkedList();

        String opStr="";

        for(int i=0;i<num_op;i++){
            opStr=scan.next();
            prevList.copy(ls);
            if(opStr.equals("I")){
                ls.insert(scan.nextInt(),scan.nextInt());
            }
            else if(opStr.equals("R")){
                ls.remove(scan.nextInt());
            }
            else{
                ls.change(scan.nextInt(),scan.nextInt());				
            }
            // Prints output
            System.out.println(ls.isBetter(prevList,K));
        }
    }
}
