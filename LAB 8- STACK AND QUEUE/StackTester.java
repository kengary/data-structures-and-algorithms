import java.util.*;

class StackTester{

	public static void main(String args[]){

		Stack<String>s=new Stack<String>();
		addToStack(s);
		//searchAndRemoveElement(s);
		//reverseStack(s);
		//sortStack();
		//MinStack ms=new MinStack();
		MergeStack<String> ms2=new MergeStack<String>();
		ms2.push(new String("Starts here: "));
		ms2.mergeStack(s);
		System.out.println(ms2); //s is empty after merging, use temp.
	}

	//add elements to stack top
	static void addToStack(Stack<String>s){
		s.push("A");
		s.push("B");
		s.push("Middle element to be removed");
		s.push("C");
		s.push("D");
		s.push("E");
		s.push("To be removed element");
	}

	//remvove last added element from stack
	static void removeFromStack(Stack s){
		s.pop();
	}
	
	//check for empty stack
	//checking top element
	static void checkStack(Stack s){
		System.out.println("Stack is empty. "+s.empty()+"\n");
		System.out.println("Top element is: "+s.peek()+"\n");
	}

	//search and remove for object in stack
	static void searchAndRemoveElement(Stack s){
		System.out.println("A returns: "+s.search("A")); //returns 6
		System.out.println("E returns: "+s.search("E")); //returns 2
		System.out.println("B returns: "+s.search("Extra Element")); //returns 1
		System.out.println("NotPresent returns: "+s.search("nil")); //returns -1

		// 1) remove all extras until the searched obj
		int count = s.search("E");
		while (count != -1 && count > 1) {
			s.pop();
			count--;
		}
		System.out.println(s+"\n");
		
		// 2) search and remove the specified element only
		s.remove("Middle element to be removed");
		System.out.println("After: "+s+"\n");
	}

	//reverse stack
	static void reverseStack(Stack s){
		Stack temp=new Stack();
		while(!s.empty())
		temp.push(s.pop());
		s=temp;
		System.out.println("REVERSED: "+s+"\n");
	}
	static void sortStack(){
		Stack<Integer> s2=new Stack<Integer>();
		Stack<Integer> temp=new Stack<Integer>();
		s2.push(2);
		s2.push(3);
		s2.push(1);
		System.out.println("int stack: "+s2);	
		Collections.sort(s2);	
		System.out.println("after sort ascending stack: "+s2);
		Collections.reverse(s2);	
		System.out.println("after sort descending stack: "+s2);	
	}
}
//return Min value frm stack
//return Max value is just reversing the > to <
class MinStack{
	Stack<Integer> minStack;
	Stack<Integer> oStack;
	public MinStack(){
		minStack=new Stack<Integer>();
		oStack=new Stack<Integer>();
	}	
	public void push(Integer e){
		if(!minStack.isEmpty()){
			int currentMin=minStack.peek();
			if(currentMin>e)minStack.push(e);else minStack.push(currentMin);
			oStack.push(e);
		}else {minStack.push(e);oStack.push(e);}
	}
	public Integer pop(){
		minStack.pop();
		return oStack.pop();
	}
	public Integer getMin(){
		return minStack.peek();
	}
}
// Merge Stack
class MergeStack<E> extends Stack<E>{
	//Your code here
	   public void mergeStack(Stack<E> otherStack)
	   {
	      //Check if stack is empty
	      if(otherStack.empty())
	         return;

	      //Take out the topmost element
	      E element = otherStack.pop();

	      //Call mergeStack for the remaining elements
	      mergeStack(otherStack);
	      this.push(element);
	   }
}
