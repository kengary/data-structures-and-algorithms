package cs1020labtester;

import java.util.*;

class Pancake {
    private boolean side1;
    private boolean side2;

    public Pancake() {
        side1 = side2 = false;
    }

    public boolean getSide1() {
        return side1;
    }

    public boolean getSide2() {
        return side2;
    }

    public boolean isSweet() {
        if (side1 || side2 == true) {
            return true;
        } else {
            return false;
        }
    }

    public void syrupOn(boolean side) {
        if (side == side1) {
            side1 = true;
        }
        if (side == side2) {
            side2 = true;
        }
    }

    public void reverseSide() {
        boolean temp;
        temp = side1;
        side2 = side1;
        side2 = temp;
    }
}

class PileOfPancake {

    Stack<Pancake> ps;

    public PileOfPancake(int N) {
        ps = new Stack<Pancake>();
        for (int i = 0; i < N; i++) {
            ps.push(new Pancake());
        }
    }

    public void flip(int indx) {
        Queue<Pancake> tq = new LinkedList<Pancake>();
        for (int i = 0; i < indx; i++) {
            ps.peek().reverseSide();
            tq.offer(ps.pop());
        }
        if (tq.peek().getSide2() == true) {
            ps.peek().syrupOn(ps.peek().getSide1());
        }
        while (!tq.isEmpty()) {
            ps.push(tq.poll());
        }
        ps.peek().syrupOn(ps.peek().getSide1());


    }

    public void add() {
        Pancake np = new Pancake();
        if (ps.peek().getSide1() == true) {
            np.syrupOn(np.getSide2());
        }
        ps.push(np);
    }

    public int count() {
        int counter = 0;
        Stack<Pancake> ts = new Stack<Pancake>();
        while (!ps.isEmpty()) {
            ts.push(ps.pop());
        }
        while (!ts.isEmpty()) {
            if (ts.peek().isSweet() == true) {
                counter++;
            }
            ps.push(ts.pop());
        }
        return counter;
    }
}

public class Main {
    public static void main(String args[]) {
        int N, Q;
        Scanner sc = new Scanner(System.in);
        N = sc.nextInt();
        Q = sc.nextInt();


        String op = "";
        if (N >= 1 && N <= 100 && Q >= 1 && Q <= 100) {
            PileOfPancake pc = new PileOfPancake(N);

            for (int i = 0; i < Q; i++) {
                op = sc.next();
                if (op.equals("ADD")) {
                    pc.add();
                } else if (op.equals("COUNT")) {
                    System.out.println(pc.count());
                } else {
                    pc.flip(sc.nextInt());
                }
            }
        }
    }
}
