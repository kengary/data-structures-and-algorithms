import java.util.*;

// write your matric number here: A0087836
// write your name here: How Wei Keng
// write list of collaborators here (reading someone's post in IVLE discussion forum and using the idea is counted as collaborating):

class HospitalTour {
	private int V; // number of vertices in the graph (number of rooms in the
					// hospital)
	// private int[][] AdjMatrix; // the graph (the hospital)
	private Vector<Integer> RatingScore; // the weight of each vertex (rating
											// score of each room)

	// if needed, declare a private data structure here that
	// is accessible to all methods in this class
	AdjList adjList;

	public HospitalTour() {
		// Write necessary codes during construction
		//
		// write your answer here
	}

	int Query() {
		int answer = adjList.getLowestRating();
		if (adjList.articulationCount == 0)
			return -1;
		else
			return answer;

	}

	// You can add extra function if needed
	// --------------------------------------------

	// --------------------------------------------
	void run() {
		Scanner sc = new Scanner(System.in);

		int TC = sc.nextInt();
		while (TC-- > 0) {
			V = sc.nextInt();

			RatingScore = new Vector<Integer>();
			adjList = new AdjList(V);

			for (int i = 0; i < V; i++)
				adjList.updateRating(i, sc.nextInt());

			for (int i = 0; i < V; i++) {
				int k = sc.nextInt();
				while (k-- > 0) {
					int j = sc.nextInt();
					adjList.addEdge(i, j, 1);
				}
			}
			System.out.println(Query());
		}
	}

	public static void main(String[] args) {
		HospitalTour ps3 = new HospitalTour();
		ps3.run();
	}
}
