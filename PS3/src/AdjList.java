import java.util.Vector;

public class AdjList {
	Vector<Vertex> list;
	int visitCounter = 0;
	int lowestRating;
	int rootChildren;
	int rootVertexValue = 0;
	int articulationCount = 0;

	public AdjList(int V) {
		list = new Vector<Vertex>();
		for (int i = 0; i < V; i++) {
			list.add(new Vertex(i));
		}
	}

	public void updateRating(int vertexValue, int rating) {
		list.get(vertexValue).rating = rating;
	}

	void addEdge(int vertexToProcess, int adjVertex, int weightOfAdjVertex) {
		list.get(vertexToProcess).adjVertexList.add(list.get(adjVertex));
	}

	void dfs(int _u) {
		Vertex u = list.get(_u);

		u.low = u.num = visitCounter++;

		for (int i = 0; i < u.adjVertexList.size(); i++) {
			Vertex v = u.adjVertexList.get(i);
			if (v.num == -1) {
				v.parent = u;

				if (u.value == rootVertexValue) {
					rootChildren++;
				}

				dfs(v.value);

				if (v.low >= u.num) {
					// articulation point.

					if (lowestRating == 0) {
						if (u.value != 0) {
							lowestRating = u.rating;
							articulationCount++;
						}
					}

					else {
						if (u.value != 0) {
							articulationCount++;
							lowestRating = Math.min(u.rating, lowestRating);
						}
					}
				}
				u.low = Math.min(u.low, v.low);
			} else {
				if (v != u.parent) {
					// v is not u's parent, i.e u reach v by back edge
					// implies that u.low = min(v.num,u.low)
					u.low = Math.min(v.num, u.low);
				}
			}
		}
	}

	int getLowestRating() {
		dfs(list.get(0).value);

		if (rootChildren > 1) {
			articulationCount++;
			if (lowestRating == 0) {
				lowestRating = list.get(0).rating;
			}
			lowestRating = Math.min(lowestRating, list.get(0).rating);
		}
		return lowestRating;
	}

	void printAdjList() {
		for (int i = 0; i < list.size(); i++) {
			System.out.print(i + " : ");
			for (int j = 0; j < (list.get(i).adjVertexList.size()); j++) {
				System.out.print(list.get(i).adjVertexList.get(j).value + ",");
			}
			System.out.println();
		}
	}
}
