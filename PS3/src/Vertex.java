import java.util.Vector;

public class Vertex {

	int num, low, value,rating;
	Vertex parent;
	Vector<Vertex> adjVertexList;

	public Vertex(int value) {
		num = low = -1;
		rating=0;
		this.value = value;
		parent = null;
		adjVertexList = new Vector<Vertex>();
	}
}
