/*
 * name	     :
 * matric no.:
 */

import java.util.*;

class Result {

    // declare the member field

    // declare the constructor

	/** generate: use this method to generate/pre-process the substrings of length K from string-idx, 
	 *           either first string or second string
	 *	   Pre-condition:
	 *	   Post-condition:
	 */

	public void generate(String dna, int idx) {
		// implementation
	}



	/** find: use this method to check whether a particular substring exists in string-idx, 
	 *       either first string, or second string
	 *	   Pre-condition:
	 *	   Post-condition:
	 */
	public boolean find(String substring, int idx) {
		// implementation
	}

}


public class Find {

	public static void main(String[] args) {

		// declare the necessary variables

		// create new object from class Result

		// declare a Scanner object to read input

		// read input and process them accordingly

	}
}