/*
* name	     :HOW WEI KENG
* matric no.: A0087836M
*/

import java.util.*;

class Result {
	private int K;
	private Hashtable<String,Integer>tb;
	// declare the member field
	public Result(int k){
		K=k;
		tb = new Hashtable<String,Integer>();
	}
	// declare the constructor

	/* generate: use this method to generate/pre-process the substrings of length K from DNA 
	*	Pre-condition:
	*	Post-condition:
	*/
	public void generate(String dna) {
		// implementation
		String key="";
		for(int i=0;i<dna.length()&&(i+K)<=dna.length();i++){
			key=dna.substring(i,i+K);
			Object obj= tb.get(key);
			if(obj==null)
			tb.put(key,new Integer(1));
			else{
				int count=((Integer) obj).intValue() + 1;
				//tb.remove(key);
				tb.put(key, new Integer(count));
			}
		}
	}

	/* count: use this method to answer one query. 
	*	Pre-condition:
	*	Post-condition:
	
	*/
	public int count(String substring) {
		// implementation
		if(tb.containsKey(substring))
		return tb.get(substring);
		return 0;
	}

}


public class DNA {

	public static void main(String[] args) {

		// declare the necessary variables
		int N=0,K=0,Q=0;
		// declare a Scanner object to read input
		Scanner sc=new Scanner(System.in);
		// create new object from class Result
		N=sc.nextInt();
		K=sc.nextInt();
		if(N>=1 && N<=500&& K>=1 && K <=6){
			Result rs=new Result(K);
			rs.generate(sc.next());
			// read input and process them accordingly
			Q=sc.nextInt();
			if(Q>=1&&Q<=1000)
			for(int i=0;i<Q;i++){
				System.out.println(rs.count(sc.next()));
			}
		}
	}
}