/*
 * author		: []
 * matric no.	: []
 */

import java.util.*;

class Schedule {
	// declare the attributes
	
	// declare the constructor

	/* clashWith: to check whether this schedule clash with a Schedule called otherSchedule
	 * 		PRE-Condition  :
	 * 		POST-Condition :
	 */
	public boolean clashWith(Schedule otherSchedule) {
		// implementation
	}
}

class Candidate {
	// declare the attributes
	
	// declare the constructor
	
	/* clashWith: to check whether this worker's schedules clash with the allocated schedule
	 * 			  of a Worker called otherCandidate
	 * 		PRE-Condition  :
	 * 		POST-Condition :
	 */
	public boolean clashWith(Candidate otherCandidate) {
		// implementation
	}
}

class HiredWorkers {
	// declare the attributes
	
	// declare the constructor

	/* count: to count hired workers with age
	 * 		PRE-Condition  :
	 * 		POST-Condition :
	 */
	public int count(int age) {
		// implementation
	}

	/* add: to add a new worker
	 * 		PRE-Condition  :
	 * 		POST-Condition :
	 */
	public void add(Candidate candidate) {
		// implementation
	}

	/* clashWith: to check whether this candidate's schedule1 and schedule2 
	 *            clash with one of the hiredWorkers.
	 * 		PRE-Condition  :
	 * 		POST-Condition :
	 */
	public boolean clashWith(Candidate candidate) {
		// implementation
	}
}

public class Main {

	public static void main(String[] args) {
		// declare the necessary variables

		// declare a Scanner object to read input

		// read input and process them accordingly

		// simulate the problem

		// output the result
	}
}