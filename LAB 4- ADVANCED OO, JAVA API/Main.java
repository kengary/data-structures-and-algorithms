/**
*	name	  :
*	matric no.:
*/

import java.util.*;

class Schedule {
	// declare the attributes
	private String _day;
	private int _startTime,_endTime;

	// declare the constructor
	public Schedule(String day,int startTime,int endTime){
		this._day=day;
		this._startTime=startTime;
		this._endTime=endTime;
	}
	public String getDay(){
		return this._day;
	}
	public int getStartTime(){
		return this._startTime;
	}
	public int getEndTime(){
		return this._endTime;
	}

	/* clashWith: to check whether this schedule clash with a Schedule called otherSchedule
	* 		PRE-Condition  :
	* 		POST-Condition :
	*/
	public boolean clashWith(Schedule otherSchedule) {
		// implementation
		if(this._day.equals(otherSchedule.getDay())){
			if( this._endTime> otherSchedule.getStartTime() &&  otherSchedule.getEndTime() > this._startTime){
				return true;
			}
		}
		return false;
	}
}
class Module {
	// declare the attributes
	private String _moduleCode;
	private Schedule _lectureSchedule,_tutorialSchedule,_labSchedule;
	private ArrayList <Schedule> _sl1;
	// declare the constructor
	public Module(String moduleCode,Schedule lectureSchedule,Schedule tutorialSchedule,Schedule labSchedule){
		this._moduleCode=moduleCode;
		this._lectureSchedule=lectureSchedule;
		this._tutorialSchedule=tutorialSchedule;
		this._labSchedule=labSchedule;
		
		this._sl1=new ArrayList<Schedule>();
		this._sl1.add(_lectureSchedule);
		this._sl1.add(_tutorialSchedule);
		this._sl1.add(_labSchedule);
	}
	public Schedule getLectSchedule(){
		return this._lectureSchedule;
	}
	public Schedule getTutSchedule(){
		return this._tutorialSchedule;
	}
	public Schedule getLabSchedule(){
		return this._labSchedule;
	}
	/* count: to count number of classes(lecture, tutorial, and lab of only this Module) on day.
	* 	      For example: when day = "Monday", lecture is on Monday, tutorial is on Monday
	*        but lab is on Tuesday, then return 2. (lecture and tutorial are on Monday).
	* 		PRE-Condition  :
	* 		POST-Condition :
	*/
	public int count(String day) {
		// implementation
		int counter=0;
		for(Schedule s1:_sl1){
			if(s1.getDay().equals(day)) {counter++;}
		}
		return counter;
	}
	public ArrayList<Schedule> getScheduleList(){
		return this._sl1;
	}
	/* clashWith: to check whether this module clash with a Module called otherModule
	* 		PRE-Condition  :
	* 		POST-Condition :
	*/
	public boolean clashWith(Module otherModule) {
		// implementation
		for(Schedule s1:_sl1){
			for(Schedule s2:otherModule.getScheduleList()){
				if(s1.clashWith(s2)){
					return true;
				}
			}
		}
		return false;
	}
}

class Timetable {
	// declare the attributes
	private ArrayList<Module> t1;

	// declare the constructor
	public Timetable(){
		t1=new ArrayList<Module>();
	}

	/* checkClash: to check whether otherModule clash with one of 
	* 			   the modules in our timetable list.
	* 		PRE-Condition  :
	* 		POST-Condition :
	*/
	public boolean checkClash(Module otherModule) {
		// implementation
		for(Module m:t1){
			if(m.clashWith(otherModule)){
				return false;//means got clashes
			}
		}
		return true;// means no clash
	}

	/* add: to add a new module to the timetable list.
	* 		PRE-Condition  :
	* 		POST-Condition :
	*/
	public String add(Module module) {
		// implementation
		if(t1.isEmpty()){
			t1.add(module);
			return "Added";
		}
		
		if(checkClash(module)){
			t1.add(module);
			return "Added";
		}else{
			return "Clashed";
		}
	}

	/* count: to count number of classes on day.
	* 		PRE-Condition  :
	* 		POST-Condition :
	*/
	public int count(String day) {
		// implementation
		int counter=0;
		for(Module m:t1){
			counter+=m.count(day);
		}
		return counter;
	}
}

class Main {

	public static void main(String[] args) {
		// declare the necessary variables
		int k=0;

		// declare a Scanner object to read input
		Scanner sc=new Scanner(System.in);

		// read input and process them accordingly
		k=sc.nextInt();
		if(k>=1 && k<=30){
			// simulate the problem
			Timetable s1=new Timetable();
			// output the result
			processInput(sc,s1,k);
		}
	}
	
	public static void processInput(Scanner sc,Timetable s1,int k){
		for(int i=0;i<k;i++){
			
			if((sc.next()).equals("MODULE")){
				String mCode=sc.next();
				Schedule lect=new Schedule(sc.next(),sc.nextInt(),sc.nextInt());
				Schedule tut=new Schedule(sc.next(),sc.nextInt(),sc.nextInt());
				Schedule lab=new Schedule(sc.next(),sc.nextInt(),sc.nextInt());
				
				Module m1=new Module(mCode,lect,tut,lab);
				
				String addModuleStatus=s1.add(m1);
				System.out.println(addModuleStatus);
				
			}else{
				int count= s1.count(sc.next());
				System.out.println(count);
			}
		}
	}
	
}
