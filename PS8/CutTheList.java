import java.util.*;

// write your matric number here:
// write your name here:
// write list of collaborators here (reading someone's post in IVLE discussion forum and using the idea is counted as collaborating):

class CutTheList {
  // if needed, declare a private data structure here that
  // is accessible to all methods in this class
  // --------------------------------------------



  public CutTheList() {
    // Write necessary codes during construction;
    //
    // write your answer here



  }

  int Query(int[] L, int N, int K) {
    int answer = 0;

    // write your answer here



    return answer;
  }

  // You can add extra function if needed



  void run() {
    // do not alter this method
    Scanner sc = new Scanner(System.in);

    int TC = sc.nextInt(); // there will be several test cases
    while (TC-- > 0) {
      // read the length N and K first
      int N = sc.nextInt(); // the length of list L
      int K = sc.nextInt(); // Esther wants to cut L into K sub-lists

      int[] L = new int[N]; // the original list L
      for (int i = 0; i < N; i++)
        L[i] = sc.nextInt();

      System.out.println(Query(L, N, K));
    }
  }

  public static void main(String[] args) {
    // do not alter this method
    CutTheList ps8 = new CutTheList();
    ps8.run();
  }
}
