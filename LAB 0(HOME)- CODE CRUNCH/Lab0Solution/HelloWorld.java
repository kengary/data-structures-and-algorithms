/**
*
* author	: []
* matric no: []
* 
*/
import java.util.*;

// Operation Class
class Operation{
	public Operation(){
		//default constructor
	}
	
	// Purpose: To evaluate the operands
	// Pre-Cond: operator not  null, operands not > 1 or < 0
	// Post-Cond: return the sum of row or col from the params.
	public String evaluate(String operator, int operand1,int operand2){
		if(operator.equals("AND")) return (operand1&operand2)+"";
		if(operator.equals("OR")) return (operand1|operand2)+"";
return "";
	}
}
class HelloWorld {

	public static void main(String[] args) {

		// declare the necessary variables
		
		int input=0; //type of reading input(1,2,3)
		Operation opsObj1=new Operation(); // a Operation object to evaluate
		String output="";

		// declare a Scanner object to read input
		Scanner sc=new Scanner(System.in);
		
		// read input and process them accordingly
		input=sc.nextInt();
		
		switch(input){
			//read N lines
		case 1: output=typeOne(opsObj1,sc);
			break;
			//read until '0'
		case 2: output=typeTwo(opsObj1,sc); 
			break;
			//read until no more
		case 3: output=typeThree(opsObj1,sc);
			break;
		default: break;
		}
		
		//print output
		System.out.print(output);
	}
	
	// Purpose: To read N number of lines
	// Pre-Cond: sc and ops initalised
	// Post-Cond: return result string 
	public static String typeOne(Operation ops, Scanner sc){
		int N=sc.nextInt();
		String result="";
		if(N<=100&&N>0){
			for(int i=0;i<N;i++){
				result+=ops.evaluate(sc.next(),sc.nextInt(),sc.nextInt())+"\n";
			}
		}
		return result;
	}
	
	// Purpose: To read until '0'
	// Pre-Cond: sc and ops initalised
	// Post-Cond: return result string 
	public static String typeTwo(Operation ops,Scanner sc){
		String result="";
		int counter=0;
		while(counter<=100&& sc.hasNext()){
		if(sc.hasNextInt()==true && sc.nextInt() == 0){
		break;
		}
			result+=ops.evaluate(sc.next(),sc.nextInt(),sc.nextInt())+"\n";
		counter++;
		}
		return result;
	}
	
	// Purpose: To read until end of file
	// Pre-Cond: sc and ops initalised
	// Post-Cond: return result string
	public static String typeThree(Operation ops,Scanner sc){
		String result="";
		int counter=0;
		while(counter<=100 && sc.hasNext()==true){
			result+=ops.evaluate(sc.next(),sc.nextInt(),sc.nextInt())+"\n";
		}
		return result;
	}
}