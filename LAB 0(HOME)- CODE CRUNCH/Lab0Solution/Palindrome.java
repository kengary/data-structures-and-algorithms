/*
* author		: []
* matric no.	: []
*/

import java.util.*;

class Palindrome {

	/* use this method to check whether the string is palindrome word or not
	* 		PRE-Condition  : word must not be null
	* 		POST-Condition : return the result as string
	*/
	public static String isPalindrome(String word) {
		if(word.length()%2==0){
			int i=0;
			int j=word.length()-1;
			
			for(;i<j;i++,j--){
				if(word.charAt(i)!=word.charAt(j)){
					return "NO";
				}
			}
			return "YES";
		}
		return "NO";
	}
	
	public static void main(String[] args) {
		// declare the necessary variables
		String s1="";
		String s2="";
		String output="";
		
		// declare a Scanner object to read input
		Scanner sc=new Scanner(System.in);

		// read input and process them accordingly
		s1=sc.next();
		s2=sc.next();
		// simulate the problem
		if(s1.length()<=100 && s2.length()<=100){
			output=isPalindrome(s1+""+s2);
		}

		// output the result
		System.out.println(output);

	}
}