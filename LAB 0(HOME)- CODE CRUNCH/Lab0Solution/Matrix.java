/**
*
* author	: []
* matric no: []
* 
*/

import java.util.*;

//Query Class
class Query{
	private int [][] _m;

	// Constructor 
	public Query(int [][] m){
	this._m=m;
	}
	
	// Purpose: To process the query
	// Pre-Cond: ops must not be null, xy not equal to zero or > M or > N
	// Post-Cond: return the sum of row or col from the params.
	public int processQuery(String ops,int xy){
		if(ops.equals("ROW")){
			return calculateRow(xy);
		}else
		return calculateCol(xy);
	}
	
	// Purpose: To process the row query
	// Pre-Cond: xy not equal to zero or > N
	// Post-Cond: return the sum of row from the params.
	private int calculateRow(int xy){
		int result=0;
		for (int i = 0; i < _m[0].length; i++) {
			result += _m[xy-1][i];
		}
		return result;
	}
	
	// Purpose: To process the col query
	// Pre-Cond: xy not equal to zero or > M 
	// Post-Cond: return the sum of col from the params.
	private int calculateCol(int xy){
	int result=0;
		for (int i = 0; i < _m.length; i++) {
			result += _m[i][xy-1];
		}
		return result;
	}

}

//Driver program
class Matrix {
	
	public static void main(String[] args) {

		// declare the necessary variables
		int N=0,M=0;
		int output=0;

		// declare a Scanner object to read input
		Scanner sc=new Scanner(System.in);
		
		// read input and process them accordingly
		N=sc.nextInt();
		M=sc.nextInt();
		
		if(N>=1&&N<=100 && M>=1 && M<=100){
			int matrix[][]=new int[N][M];
			matrix=readMatrix(sc,N,M);
			
			Query q1=new Query(matrix);
			output=q1.processQuery(sc.next(),sc.nextInt());
		}
		
		//print output
		System.out.print(output);

	}
	
	// Purpose: static helper method to read the matrix
	// Pre-Cond: sc initialised, N and M not equal to 0
	// Post-Cond: return the filled matrix	
	public static int[][] readMatrix(Scanner sc,int N,int M){
		int m1[][]=new int[N][M];
		for(int i=0;i<N;i++){
			for(int j=0;j<M;j++){
				m1[i][j]=sc.nextInt();
			}
		}
		return m1;
	}
}