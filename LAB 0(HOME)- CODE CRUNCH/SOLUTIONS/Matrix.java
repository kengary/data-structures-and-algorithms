import java.util.Scanner;

/**
 *
 * @author Wei Keng
 */
public class Matrix {

    private int maxInput = 100, minInput = 1;

    public void runSolution() {
// RUN point
        try {
            System.out.println(matrixResult());
            System.exit(0);
        } catch (Exception ex) {
            System.out.println(ex + " in RUN.");
        }
    }

    public int matrixResult() {
        //return the matrix eval result
        int rowN, columnM, queryNum, outputResult = 0;
        int[][] storeArray;
        String queryType;
        try {
            Scanner scan = new Scanner(System.in);
            rowN = scan.nextInt();
            columnM = scan.nextInt();
            //validate max and min row and col input and store in array
            if (rowN > maxInput && rowN < minInput || columnM > maxInput && columnM < minInput) {
                System.out.println("Error in MAX and MinInput");
                System.exit(0);
            } else {
                storeArray = new int[rowN][columnM];
                for (int i = 0; i < rowN; i++) {
                    for (int j = 0; j < columnM; j++) {
                        storeArray[i][j] = scan.nextInt();
                    }
                }
                queryType = scan.next();
                queryNum = scan.nextInt();
//calculate result
                if (queryType.equalsIgnoreCase("row")) {
                    for (int i = 0; i < columnM; i++) {
                        outputResult += storeArray[queryNum - 1][i];
                    }

                } else if (queryType.equalsIgnoreCase("column")) {
                    for (int i = 0; i < rowN; i++) {
                        outputResult += storeArray[i][queryNum - 1];
                    }
                }
            }
        } catch (Exception ex) {
            System.out.println(ex + " in matrix Results.");
        }
        return outputResult;
    }

    public static void main(String[] args) {
        // TODO code application logic here
        Matrix m1 = new Matrix();
        m1.runSolution();
    }
}
