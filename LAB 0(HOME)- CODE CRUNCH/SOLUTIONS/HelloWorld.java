import java.util.Scanner;

/**
 *
 * @author Wei Keng
 */
public class HelloWorld {

    private Scanner scan = new Scanner(System.in);
private int numOfProcess=100;
    public void runSolution() {
// Constructor
        try {
            readInputType();
            System.exit(0);
        } catch (Exception ex) {
            System.out.println(ex + " in Constructor.");
        }
    }

    private int printOperationResult() {
        // Scan for OperatorType and OperandBits and evaluate results
        String operatorType = "";
        int operandBits1 = 0, operandBits2 = 0, result = 0;
        try {
            operatorType = scan.next();
            operandBits1 = scan.nextInt();
            operandBits2 = scan.nextInt();
            if (operatorType.equalsIgnoreCase("AND")) {
                result = operandBits1 & operandBits2;
            } else if (operatorType.equalsIgnoreCase("OR")) {
                result = operandBits1 | operandBits2;
            } else {
                System.out.println("Error in loop");
            }
        } catch (Exception ex) {
            System.out.println("Exception in OperationEval: " + ex);
        }
        return result;
    }

    private void readInputType() {
        // Switch Case based on type of reading input 1,2,3
        int typeOfReadingInput, outputResult, numOfOperationN,counter=0;
        typeOfReadingInput = scan.nextInt();
        //Scanner scan = new Scanner(System.in);
        switch (typeOfReadingInput) {
            case 1:
                try {
                    numOfOperationN = scan.nextInt();
                    for (int i = 0; i < numOfOperationN; i++) {
                        if(i<numOfProcess){
                        outputResult = printOperationResult();
                        System.out.println(outputResult);
                        }else{System.exit(0);}
                        
                    }
                } catch (Exception ex) {
                    System.out.println("Exception in 1: " + ex);
                }

                break;
            case 2:
                try {
                    do {
                        if (scan.hasNextInt() == true) {
                            if (scan.nextInt() == 0) {
                                System.exit(0);
                            } else {
                                outputResult = printOperationResult();
                                System.out.println(outputResult);
                            }
                        } else {
                            outputResult = printOperationResult();
                            System.out.println(outputResult);
                        }
counter++;
                    } while (scan.hasNext()&&counter<numOfProcess);
                } catch (Exception ex) {
                    System.out.println("Exception in 2: " + ex);
                }
                break;
            case 3:
                try {
                    do {
                        outputResult = printOperationResult();
                        System.out.println(outputResult);
                        counter++;
                    } while (scan.hasNext()&&counter<numOfProcess);
                } catch (Exception ex) {
                    System.out.println("Exception in 3: " + ex);
                }
                break;
            default:
                System.out.println("Error in Switch");
        }
    }

    public static void main(String[] args) {
        // TODO code application logic here
        HelloWorld p1 = new HelloWorld();
        p1.runSolution();
    }
}
