package BabyNames;

//write your matric number here: A0087836
//write your name here: How Wei Keng
//write list of collaborators here (reading someone's post in IVLE discussion forum and using the idea is counted as collaborating):
//Professor Steven Halim's Lecture notes and BST.java
class AVLVertex {

	AVLVertex parent, left, right;
	int height;
	Baby key;

	public AVLVertex(Baby v) {
		this.key = v;
		this.parent = this.left = this.right = null;
		this.height = 0;
	}
}