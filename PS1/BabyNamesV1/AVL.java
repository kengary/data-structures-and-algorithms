package BabyNames;

//write your matric number here: A0087836
//write your name here: How Wei Keng
//write list of collaborators here (reading someone's post in IVLE discussion forum and using the idea is counted as collaborating):
//Professor Steven Halim's Lecture notes and BST.java
class AVL {
	protected AVLVertex root;

	public AVL() {
		root = null;
	}

	/**
	 * Helper method to insert Baby Objects
	 * 
	 * @param T
	 *            the Baby object to be inserted
	 * */
	public void insert(Baby v) {
		root = insert(root, v);
	}

	/**
	 * Insert new Baby into AVL
	 * 
	 * @param T
	 *            the vertex to start insertion
	 * @param v
	 *            Baby object to be inserted
	 * @return the updated tree's root
	 * */
	protected AVLVertex insert(AVLVertex T, Baby v) {
		if (T == null)
			return new AVLVertex(v); // insertion point is found

		if (T.key.name.compareTo(v.name) < 0) { // search to the right
			T.right = insert(T.right, v);
			T.right.parent = T;

		} else { // search to the left
			T.left = insert(T.left, v);
			T.left.parent = T;
		}
		T = checkAndRebalance(T);
		T.height = Math.max(heightOf(T.left), heightOf(T.right)) + 1;

		return T; // return the updated BST
	}

	/**
	 * Check the balance factor of vertex of the tree to balance it
	 * 
	 * @param T
	 *            the vertex to be check/balance
	 * @return the new vertex that has been balanced
	 * */
	private AVLVertex checkAndRebalance(AVLVertex T) {

		int bf = heightOf(T.left) - heightOf(T.right);

		if (bf == 2) {
			if (heightOf(T.left.left) - heightOf(T.left.right) == 1)
				T = rotateRight(T);
			else {
				T.left = rotateLeft(T.left);
				T = rotateRight(T);
			}
		} else if (bf == -2) {
			if (heightOf(T.right.left) - heightOf(T.right.right) == -1)
				T = rotateLeft(T);
			else {
				T.right = rotateRight(T.right);
				T = rotateLeft(T);
			}
		} else {
			// do nothing
		}

		return T;
	}

	/**
	 * Check vertex's existence and return its height
	 * 
	 * @param T
	 *            the vertex to check
	 * @return height of vertex if exist else return -1
	 * */
	protected int heightOf(AVLVertex T) {
		return T == null ? -1 : T.height;
	}

	/**
	 * Left Rotate the Vertex of the tree to balance it
	 * 
	 * @param T
	 *            the vertex to be rotated
	 * @return the new vertex that replaced the old that has been rotated left
	 * */
	protected AVLVertex rotateLeft(AVLVertex T) {

		AVLVertex w = T.right;
		w.parent = T.parent;
		T.parent = w;
		T.right = w.left;

		if (w.left != null)
			w.left.parent = T;

		w.left = T;

		T.height = Math.max(heightOf(T.left), heightOf(T.right)) + 1;
		w.height = Math.max(heightOf(w.left), heightOf(w.right)) + 1;

		return w;
	}

	/**
	 * Right Rotate the Vertex of the tree to balance it
	 * 
	 * @param T
	 *            the vertex to be rotated
	 * @return the new vertex that replaced the old that has been rotated right
	 * */
	protected AVLVertex rotateRight(AVLVertex T) {

		AVLVertex w = T.left;
		w.parent = T.parent;
		T.parent = w;
		T.left = w.right;

		if (w.right != null)
			w.right.parent = T;

		w.right = T;

		T.height = Math.max(heightOf(T.left), heightOf(T.right)) + 1;
		w.height = Math.max(heightOf(w.left), heightOf(w.right)) + 1;

		return w;
	}

	/**
	 * Recursive Search for number of baby names in range in avl tree
	 * 
	 * @param T
	 *            node currently visiting
	 * @param START
	 *            start of the range(inclusive)
	 * @param END
	 *            end of the range(exclusive)
	 * @param gen
	 *            genderSuitability
	 * @return number of baby names in range and correct gender
	 * */
	protected int inorderCount(AVLVertex T, String Start, String End, int gen) {
		if (T == null)
			return 0;
		else if (T.key.name.compareTo(Start) < 0)
			// if name is before Start,search right
			return inorderCount(T.right, Start, End, gen);

		else if (T.key.name.compareTo(End) >= 0)
			// if name is after End,search left
			return inorderCount(T.left, Start, End, gen);

		else {
			// if name falls in range, validate genderSuitability,+1 if node
			// match
			if (gen == 0) {
				return (inorderCount(T.left, Start, End, gen)
						+ inorderCount(T.right, Start, End, gen) + 1);
			} else if (gen == 1) {
				return (T.key.gender == 1 ? inorderCount(T.left, Start, End,
						gen) + inorderCount(T.right, Start, End, gen) + 1
						: inorderCount(T.left, Start, End, gen)
								+ inorderCount(T.right, Start, End, gen));
			} else {
				return (T.key.gender == 2 ? inorderCount(T.left, Start, End,
						gen) + inorderCount(T.right, Start, End, gen) + 1
						: inorderCount(T.left, Start, End, gen)
								+ inorderCount(T.right, Start, End, gen));
			}

		}

	}

	/**
	 * Helper method for inorder(..)
	 * 
	 * @param START
	 *            start of the range(inclusive)
	 * @param END
	 *            end of the range(exclusive)
	 * @param gen
	 *            genderSuitability
	 * @return number of baby names in range and correct gender
	 * */
	public int inorder(String START, String END, int gen) {
		return inorderCount(root, START, END, gen);
	}

}