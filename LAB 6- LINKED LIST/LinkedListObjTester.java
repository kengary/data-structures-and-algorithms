import java.util.*;

public class LinkedListObjTester{
	public static void main(String args[]){
		//declare linked list
		LinkedList<String> list1=new LinkedList<String>();

		//add elements to linked list
		list1.add("First");
		list1.add("2");
		list1.add("3");
		list1.add("4");
		list1.add("5");
		list1.add("Last");

		//set element in linked list
		list1.add("Last");
		list1.set(5,"6");

		//foreach looping print all elements
		for(String str:list1)
		System.out.print(str+" ");
		System.out.println();

		//create new sublist
		List<String> subList1=new LinkedList<String>(list1.subList(1,6));
		for(String str:subList1)
		System.out.print(str+" ");
		System.out.println();

		//clear from sublist range
		subList1.subList(1,3).clear();
		for(String str:subList1)
		System.out.print(str+" ");
		System.out.println();

		//addding new element at specific index(first and last)
		subList1.add(0,"First");
		subList1.add(subList1.size(),"Last");
		for(String str:subList1)
		System.out.print(str+" ");
		System.out.println();

		//clear the link list
		//subList1.clear();

		//remove specific element from list
		String i= subList1.remove(1);
		boolean j=subList1.remove("1");
		System.out.print(i + " " + j);
		System.out.println();

		//convert linked list to array list
		List<String> arrayList1=new ArrayList<String>(subList1);
		for(String str: arrayList1)
		System.out.print(str+" ");
		System.out.println();

		//check if linkedlist contains value
		boolean k=subList1.contains("5");
		System.out.print(k);
		System.out.println();

		//check for the position of an element
		int l=subList1.indexOf("5");
		System.out.println(l);
		System.out.println();

		//use ListIterators
		ListIterator<String> listIter=list1.listIterator();

		//Looping thru linked list forward--* once looped, cannot be reuse
		while(listIter.hasNext())
		System.out.print(listIter.next()+" ");
		System.out.println();

		//Looping thru linked list reverse USING ListIterator
		ListIterator<String> listIterReverse=list1.listIterator(list1.size());
		while(listIterReverse.hasPrevious())
		System.out.print(listIterReverse.previous()+" ");
		System.out.println();

		//Looping thru linked list reverse USING Iterator
		Iterator<String> prtReverse=list1.descendingIterator();
		while(prtReverse.hasNext())
		System.out.print(prtReverse.next()+" ");
		System.out.println();

		//Adding element to linked list using iterator
		ListIterator<String> listIter2=list1.listIterator();
		listIter2.add("Added Element");

		//Removing element frm linked list and print after removed list
		ListIterator<String> listIterRemove=list1.listIterator();
		while(listIterRemove.hasNext()){
			String str1=listIterRemove.next();
			if(str1.equals("Added Element")){
				listIterRemove.remove();
			}else{
				System.out.print(str1+" ");
			}
		}


		//Replacing element and print after replaced list
		ListIterator<String> listIterReplace=list1.listIterator();
		while(listIterReplace.hasNext()){
			String str1=listIterReplace.next();
			if(str1.equals("Last")){
				listIterReplace.set("Final");
				str1=listIterReplace.previous();
				str1=listIterReplace.next();
			}
			System.out.print(str1+" ");
		}
		
		System.out.println();
		System.out.println("//===========End of Basic");
		System.out.println();
		
		
		//LinkedList object==========================================		
		
		LinkedListObjTester llot=new LinkedListObjTester();
		Contact c1=new  Contact("Aat","Gah",20, 01);
		Contact c2=new  Contact("Ban","Gbh",28, 02);
		Contact c3=new  Contact("Cart","Gch",27,03);
		Contact c4=new  Contact("Dan","Gdh",24, 04);
		Contact c5=new  Contact("Ean","Geh",25, 05);
		Contact c6=new  Contact("Fan","Gfh",21, 06);
		Contact testC=new  Contact("TEST","TESTR",27, 07);
		llot.add(c1);
		llot.add(c2);
		llot.add(c3);
		llot.add(c4);
		llot.add(c5);
		llot.add(c6);
		//llot.addAfter(c1,testC);
		//llot.addBefore(c1,testC);
		//llot.rmAfter(c2);
		//llot.rmBefore(c2);
		//llot.searchAndReplace(c1,testC);
		//llot.sortAscending();
		//llot.sortDescending();
		//llot.reverse();
		//llot.swap(2);
		
		//llot.join2();
		llot.rmDuplicate();
		System.out.println(llot);
	}	
	
	private LinkedList<Contact> addressBook;
	public LinkedListObjTester(){
		addressBook=new LinkedList<Contact>();
		
	}
	public void add(Contact c){
		addressBook.add(c);
	}
	//join 2 linked list
	public void join2(){	
		addressBook.addAll(addressBook);
	}
	
	//remove duplicates
	public void rmDuplicate(){
		Set<Contact> mySet = new HashSet<Contact>(addressBook);
		addressBook=new LinkedList<Contact>(mySet);
	}
	
	//add after 
	public void addAfter(Contact b4c,Contact c){
		int i=addressBook.indexOf(b4c);
		addressBook.add(i+1,c);	
	}
	//add before 
	public void addBefore(Contact b4c,Contact c){
		int i=addressBook.indexOf(b4c);
		addressBook.add(i,c);	
	}
	
	//remove after 
	public void rmAfter(Contact c){
		int i=addressBook.indexOf(c) + 1;
		if(i<addressBook.size())
		addressBook.remove(i);	
	}
	//remove before
	public void rmBefore(Contact c){
		int i=addressBook.indexOf(c) - 1;
		if(i>-1)
		addressBook.remove(i);	
	}
	
	//Search and replace element
	public void searchAndReplace(Contact sc,Contact c){
		ListIterator<Contact> listIterReplace=addressBook.listIterator();
		while(listIterReplace.hasNext()){
			Contact str1=listIterReplace.next();
			if(str1==sc){
				listIterReplace.set(c);
			}
		}
	}
	//reverse Linkedlist
	public void reverse(){
		Collections.sort(addressBook, Collections.reverseOrder());
	}
	
	//Sort linkedlist by numeric ascending
	public void sortAscending(){
		Collections.sort(addressBook,new sortByAscending());
	}
	
	//Sort linkedlist by numeric descending
	public void sortDescending(){
		Collections.sort(addressBook,new sortByDescending());
	}
	
	public void sortName(){
		Collections.sort(addressBook,new sortByName());
	}
	
	//Swaping with next element
	public void swap(int index){
		Contact temp=addressBook.get(index);
		addressBook.set(index,addressBook.get(index+1));
		addressBook.set((index+1),temp);
	}
	
	public String toString(){
		String str="";
		ListIterator<Contact> li=addressBook.listIterator();
		while(li.hasNext()){
			str+=li.next().toString();
		}
		return str;
	}	
}
class sortByAscending implements Comparator<Contact>{
	public int compare(Contact A,Contact B){
		return A.getAge()-B.getAge();
	}
}
class sortByDescending implements Comparator<Contact>{
	public int compare(Contact A,Contact B){
		return B.getAge()-A.getAge();
	}
}
class sortByName implements Comparator<Contact>{
	public int compare(Contact A,Contact B){
		return (A.getLast()).compareTo(B.getLast());
	}
}

class Contact implements Comparable<Contact>{
	private String _lastName;
	private String _firstName;
	private int _age;
	private int _id;
	public Contact(String last,String first,int age, int id){
		_lastName=last;
		_firstName=first;
		_age=age;
		_id=id;
	}
	public int compareTo(Contact c){
		return _id-c.getID();
	}
	public String toString(){
		String str="ID: "+ _id+"\n";
		str+="Name: "+ _lastName + " "+ _firstName+"\n";
		str+="Age: "+ _age +"\n\n";
		return str;
	}
	public String getLast(){
		return _lastName;
	}
	public String getFirst(){
		return _firstName;
	}
	public int getAge(){
		return _age;
	}
	public int getID(){
		return _id;
	}
}

