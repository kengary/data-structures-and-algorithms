import java.util.*;

public class LLtest{
	public static void main(String args[]){
		LinkedList<String> ll=new LinkedList<String>();
		ll.add("1");
		ll.add("2");
		ll.add("3");
		ll.add("4");
		ll.add("5");
		ll.add("tested");

System.out.println("\n Using LinkedList Api:\n");
		/*
System.out.println("First element: "+ll.getFirst());
System.out.println("Last element: "+ll.getLast());

System.out.println("Foreach looping: ");
for(String str:ll)
System.out.println(str);

System.out.println("\nSublist: ");
List<String> subbed= ll.subList(0,4);

for(String str:subbed)
System.out.println("Sublist: "str);

System.out.println("\n Clear a range from the linked list: ");
ll.subList(1,2).clear();
for(String str:ll)
System.out.println(str);


System.out.println("\nAdding new element at specific index: ");
ll.add(0,"Added Element");
for(String str:ll)
System.out.println(str);

System.out.println("\n Clear the linkedlist: ");
ll.clear();
for(String str:ll)
System.out.println(str);


System.out.println("\n Remove specified element frm linked list: ");
ll.remove("tested");
ll.remove(1);
for(String str:ll)
System.out.println(str);


System.out.println("convert linked list to array list");
List<String> myList= new ArrayList<String>(ll);
for(String str:myList)
System.out.println(str);


System.out.println(ll.indexOf("0"));
*/

//==========================================================================
/*
System.out.println("\n Using Iterators:\n");

System.out.println("Iterator looping forward direction");
ListIterator<String> liNext=ll.listIterator();
while(liNext.hasNext()){
System.out.println(liNext.next());
}

Iterator<String> lp=ll.descendingIterator();
while(lp.hasNext()){
System.out.println(lp.next());
}


System.out.println("\n Iterator looping backward direction");
ListIterator<String> liPre=ll.listIterator(ll.size());
while(liPre.hasPrevious()){
System.out.println(liPre.previous());
}

System.out.println("\n Adding element using iterator");
	ListIterator<String> listIterator = ll.listIterator();
	listIterator.next();
	listIterator.add("Added Element");
	for (String str: ll){
	System.out.println(str);
	}


System.out.println("\n Removing elemenet using iterator");
ListIterator<String> lr=ll.listIterator();
while(lr.hasNext()){
String curr=lr.next();
if(curr.equals("Added Element")){
lr.remove();
}
System.out.println("test here: "+curr);
}



System.out.println("Replacing Element using iterator: ");
ListIterator<String> ls=ll.listIterator();
while(ls.hasNext()){
ls.next();
ls.set("100");
}
for (String str: ll){
	System.out.println(str);
	}
*/
	}
}