/**
*
* author	: [HOW WEI KENG]
* matric no: [A0087836M]
* 
*/

// import important packages
import java.util.*;

class Result {

	// declare the member field
	private int _counter;
	private Queue<Integer> treePath;
	private Stack<Integer> tallTree;

	// declare the constructor
	public Result(){
		treePath=new LinkedList<Integer>();
		tallTree=new Stack<Integer>();
		_counter=0;
	}
	/* solve: to compute the result, return the result
	* 		PRE-condition:-
	*		POST-condition:add tree to the tree path queue
	*/
	public void addTree(int height){
		treePath.offer(height);
	}

	/* solve: to compute the result, return the result
	* 		PRE-condition:-
	*		POST-condition:return counts of paired trees
	*/
	public int solve() {
		// implementation
		
		while(!treePath.isEmpty()){
			//if there is no record of tall tree
			//push into record, the tree in the path
			if(tallTree.isEmpty())
			tallTree.push(treePath.poll());
			//if a tree in the path is shorter than tree in the record
			//push the tree in the path into the record
			// counter + 1
			else if(treePath.peek()<tallTree.peek()){
				tallTree.push(treePath.poll());
				_counter++;
			}
			//if a tree in the path is taller than tree in the record
			//pop the tree in the record out
			//conter + 1
			else if(treePath.peek()>tallTree.peek()){
				tallTree.pop();
				_counter++;
			}
			//tree in path is equal to tree in record
			// counter + 1
			else{
				treePath.poll();
				_counter++;
			}
			
		}
		return _counter;	
	}
}

public class Swing {

	public static void main(String[] args) {

		// declare the necessary variables
		int N,ai;
		
		// create new object from class Result
		Result rs=new Result();
		
		// declare a Scanner object to read input
		Scanner sc=new Scanner(System.in);
		
		// read input and process them accordingly
		N=sc.nextInt();
		if(N>=2&&N<=50000){
			for(int i=0;i<N;i++){
				ai=sc.nextInt();
				if(ai>=0&&ai<=(Math.pow(2,31)))
				rs.addTree(ai);
			}
			//process and print output
			System.out.println(rs.solve());
		}
	}
}