/**
 *
 * author	: []
 * matric no: []
 * 
 */

// import important packages

class Result {

    // declare the member field

    // declare the constructor

	/* doP: implement P command
	 * 		PRE-condition:
	 *	 	POST-condition:
	 */
	public void doP(char character) {
		// implementation
	}

	/* doL: implement L command
	 * 		PRE-condition:
	 *		POST-condition:
	 */
	public void doL() {
		// implementation
	}

	/* doD: implement D command
	 * 		PRE-condition:
	 *		POST-condition:
	 */
	public void doD() {
		// implementation
	}

	/* doB: implement B command
	 * 		PRE-condition:
	 *		POST-condition:
	 */
	public void doB() {
		// implementation
	}

	// implement other necessary methods

}

public class Editor {

	public static void main(String[] args) {        
		// declare the necessary variables

		// create new object from class Result

		// declare a Scanner object to read input

		// read input and process them accordingly
	}
}