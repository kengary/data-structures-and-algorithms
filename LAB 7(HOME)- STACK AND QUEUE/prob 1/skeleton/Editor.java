/**
 *
 * author	: [HOW WEI KENG]
 * matric no: [a0087836M]
 * 
 */

// import important packages
import java.util.*;
class Result {

    // declare the member field
    Stack<Character> left;
    Stack<Character> right;

    // declare the constructor
    public Result(){
        left=new Stack<Character>();
        right=new Stack<Character>();
    }

    /* doP: implement P command
     * 		PRE-condition: c must not be null
     *	 	POST-condition: push c in to left stack
     */
    public void doP(char c) {
        // implementation
        left.push(c);
    }

    /* doL: implement L command
     * 		PRE-condition:-
     *		POST-condition:push left-stack's pop to right stack
     */
    public void doL() {
        // implementation
        if(left.empty()){
            return;
        }

        right.push(left.pop());
    }

    /* doD: implement D command
     * 		PRE-condition:-
     *		POST-condition:push right-stack's pop to left stack
     */
    public void doD() {
        // implementation
        if(right.empty()){			
            return;
        }

        left.push(right.pop());
    }

    /* doB: implement B command
     * 		PRE-condition:-
     *		POST-condition:return the char being deleted from the left stack.
     */
    public char doB() {
        // implementation
        if(left.empty()){
            return 0;
        }
        return left.pop();
    }

    // implement other necessary methods
    /* toString: override toString 
     * 		PRE-condition:-
     *		POST-condition:return the resultant string
     */
    public String toString(){
        String result="";
        while(!left.empty())
            right.push(left.pop());

        while(!right.empty())
            result+=right.pop();
        return result;
    }

}

public class Editor {

    public static void main(String[] args) {        
        // declare the necessary variables
        String str;
        int N;
        // create new object from class Result
        Result rs=new Result();
        // declare a Scanner object to read input
        Scanner sc=new Scanner(System.in);
        // read input and process them accordingly
        str=sc.next();
        if(str.length()<=10000){

            for(int i=0;i<str.length();i++)
                rs.doP(str.charAt(i));

            N=sc.nextInt();
            for(;N<=20000&&N>0;N--){
                String op=sc.next();
                if(op.equals("P"))
                    rs.doP((sc.next()).charAt(0));
                else if(op.equals("L"))
                    rs.doL();
                else if(op.equals("D"))
                    rs.doD();
                else
                    rs.doB();
            }
            System.out.println(rs.toString());	
        }
    }

}
