/**
 *
 * author	: [HOW WEI KENG]
 * matric no: [A0087836M]
 * 
 */

import java.util.*;

//Bus object with bus capacity and Group array as attributes
class Bus{
    private final int _capacity;
    private int _availSeat;
    private Vector<Group> _grpArr;

    //constr
    public Bus(int capacity){
        _capacity=capacity;
        _availSeat=capacity;
        _grpArr=new Vector<Group>();
    }

    public int getCapacity(){
        return _capacity;
    }
    public Vector<Group> getGroupArr(){
        return _grpArr;
    }
    public int getAvailSeat(){
        return _availSeat;
    }
    public Group getGroupInBus(int index){
        return _grpArr.get(index);
    }
    public void takeGrp(Group grp){
        _grpArr.add(grp);
        _availSeat-=grp.getNumOfPpl();
    }

}
// Group object with number of people and token as attributes.
class Group {

    // declare the member field
    private final int _num_ppl;
    private final int _token_num;

    // declare the constructor
    public Group(int num_ppl,int token_num){
        _num_ppl=num_ppl;
        _token_num=token_num;
    }
    // implement other necessary methods
    public int getNumOfPpl(){
        return _num_ppl;
    }
    public int getTokenNum(){
        return _token_num;
    }
}

class BusService {

    // declare the member field
    private Queue <Group> grpQueue;

    // declare the constructor
    public BusService(){
        grpQueue=new LinkedList<Group>();
    }

    /* addNewGroup: to add a new group to our queue.
     * 		PRE-condition:num_ppl and token are not negative values
     *		POST-condition:add the grp to the queue
     */
    public void addNewGroup(int num_ppl,int token_num) {
        // implementation
        grpQueue.offer(new Group(num_ppl,token_num));		
    }

    /* busArrival: a new bus come and determine the eligible visitors to take the bus.
     * 		PRE-condition:capacity is > 0
     *		POST-condition:returns Empty Bus. if empty or returns 1st grp token, last grp token, num_visitor in bus if !empty 
     */
    public String busArrival(int capacity) {
        // implementation
        Bus newBus=new Bus(capacity);
        while(!grpQueue.isEmpty()&&newBus.getAvailSeat()>=(grpQueue.peek()).getNumOfPpl()){
            newBus.takeGrp(grpQueue.poll());
        }				
        if((newBus.getCapacity()-newBus.getAvailSeat())==0)
            return "Empty Bus.";
        else
            return ""+newBus.getGroupArr().firstElement().getTokenNum()+" "+newBus.getGroupArr().lastElement().getTokenNum()+" "+(newBus.getCapacity()-newBus.getAvailSeat());
    }
    // implement other necessary methods
}

public class Airshow {

    public static void main(String[] args) {
        // declare the necessary variables
        int N;

        // create new object from class Result
        BusService bs=new BusService();

        // declare a Scanner object to read input
        Scanner sc=new Scanner(System.in);

        N=sc.nextInt();

        // read input and process them accordingly
        for(int i=0;i<N;i++){
            String op=sc.next();
            if(op.equals("Bus"))
                System.out.println(bs.busArrival(sc.nextInt()));		
            else 
                bs.addNewGroup(sc.nextInt(),sc.nextInt());		
        }
    }
}
