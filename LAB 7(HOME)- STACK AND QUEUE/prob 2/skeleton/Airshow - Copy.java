/**
 *
 * author	: []
 * matric no: []
 * 
 */

import java.util.*;

// Visitor object with number of people and token as attributes.
class Visitor {

    // declare the member field

    // declare the constructor

	// implement other necessary methods

}

class BusService {

    // declare the member field

    // declare the constructor


	/* addNewGroup: to add a new group to our queue.
	 * 		PRE-condition:
	 *		POST-condition:
	 */
	public void addNewGroup(Visitor visitor) {
		// implementation
	}


	/* busArrival: a new bus come and determine the eligible visitors to take the bus.
	 * 		PRE-condition:
	 *		POST-condition:
	 */
	public String busArrival(int capacity) {
		// implementation
	}
	// implement other necessary methods
}

public class Airshow {

	public static void main(String[] args) {
		// declare the necessary variables

		// create new object from class Result

		// declare a Scanner object to read input

		// read input and process them accordingly
	}
}