import java.util.*;

class Game{

    public static void main(String[] args){
		// declare the necessary variables

		// declare a Scanner object to read input

		// read input and process them accordingly
    }
}

class Square{

    // declare the member field

    // declare the constructor

	// declare accessor method to return the member field

	/* changePoints: update the points based on current position
	 * 		PRE-condition:
	 *		POST-condition:
	 */
    public double changePoints(double curPoints){
    }
}

class SpecialSquare extends Square{
	
    // declare the member field

    // declare the constructor
	
	// declare accessor method to return the member field

	/* changePoints: update the points based on current position and multiplier
	 * 		PRE-condition:
	 *		POST-condition:
	 */
    public double changePoints(double curPoints){
    }
}