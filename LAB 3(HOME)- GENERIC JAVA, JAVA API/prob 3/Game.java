/*
 * author       : [How Wei Keng]
 * matric no.  : [A0087836]
 */
import java.util.*;

class Square{

    // declare the member field
    // weight for square value
    protected double _value;

    // declare the constructor
    public Square(double value){
        _value=value;
    }

    // declare accessor method to return the member field

    // return weight value for the square
    public double getValue(){
        return _value;
    }

    /* changePoints: update the points based on current position
     * 		PRE-condition: 
     *		POST-condition:return score which contains the accumulated
     *		points
     */
    public double changePoints(double curPoints){
        double score=0;
        score=curPoints+this.getValue();
        return score;
    }
}

// subClass with additional multiplier value
class SpecialSquare extends Square{

    // declare the member field
    protected double _multiplier;

    // declare the constructor
    public SpecialSquare(double value,double multiplier){
        super(value);
        _multiplier=multiplier;
    }

    // declare accessor method to return the member field
    public double getMultiplier(){
        return _multiplier;
    }

    /* changePoints: update the points based on current position and multiplier
     * 		PRE-condition:-
     *		POST-condition:return the score which contains the
     *		accumulated score
     */
    public double changePoints(double curPoints){
        double score=0.0;
        score=(curPoints*this.getMultiplier())+this.getValue();
        return score;
    }
}
//driver class
class Game{
    public static void main(String[] args){

        // declare the necessary variables
        //curIndex -the current index of matrix after moving to a
        //square

        int numOfSquare=0,curIndex=0;
        double score=0.0;

        // declare a Scanner object to read input
        Scanner input=new Scanner(System.in);
        numOfSquare=input.nextInt();

        // Create new Square datatype matrix to store Square objs
        Square [] gameSquare= new Square[numOfSquare];

        // read input and process them accordingly
        // call static createSquare method to fill matrix
        for(int i=0;i<numOfSquare;i++){
            createSquareType(input,gameSquare,i);
        }

        // process the game

        while(input.hasNextInt()){
            int diceNum=0;
            diceNum=input.nextInt();

            // current index + dice number to move to new square
            curIndex+=diceNum;

            // Algo determine new square index if > numOfSquare
            curIndex=(curIndex%numOfSquare);

            //get the new score>depends on square type;
            score= processDiceNum(input,score,gameSquare,curIndex);
        }
        //print out results.
        System.out.printf("Final score: %.2f\n",score);
    }

    // static method to fill the Square obj matrix
    public static void createSquareType(Scanner input,Square []
            gameSquare,int index){
        double multiplier;
        double value;
        if(input.nextInt()==0){
            value=input.nextDouble();
            Square sq=new Square(value);
            gameSquare[index]=sq;
        }else{
            value=input.nextDouble();
            multiplier=input.nextDouble();
            SpecialSquare ssq=new SpecialSquare(value,multiplier);
            gameSquare[index]=ssq;
        }
    }
    //static method to process the dice num and return the score
    public static double processDiceNum(Scanner input,double
            score,Square [] gameSquare,int curIndex){
        double curScore=0.0;

        curScore=gameSquare[curIndex].changePoints(score);
        
        return curScore;
    }
}
