import java.util.*;

class Result {
	// declare the member field

	// declare the constructor

	// declare accessor method to return the member field

	/* add the requirements given
	 * 		PRE-condition:
	 *	 	POST-condition:
	 */
	public void addR(String requirement) {

	}
		
	/* determine which proposals has the best criteria.
	 * 		PRE-condition:
	 *		POST-condition:
	 */
	public String bestProposal(List<Proposal> proposals) {

	}
}

class Proposal {
	
    // declare the member field

    // declare the constructor
	
	// declare accessor method to return the member field
	
	/* implement getPrice which will return the price of the corresponding proposal
	 * 		PRE-condition:
	 *		POST-condition:
	 */
	public int getPrice() {

	}
	
	/* implement getName which will return the name of the corresponding proposal
	 * 		PRE-condition:
	 *		POST-condition:
	 */
	public String getName() {
	
	}
	
	/* implement addRequirement which will add the requirement of the corresponding proposal
	 * 		PRE-condition:
	 *		POST-condition:
	 */
	public void addRequirement(String requirement) {

	}
	
	/* implement getRequirement which will get the i-th requirement of the corresponding proposal
	 * 		PRE-condition:
	 *		POST-condition:
	 */
	public String getRequirement(int index) {

	}
		
	/* implement getTotalRequirement which will get the total requirements of the corresponding proposal
	 * 		PRE-condition:
	 *		POST-condition:
	 */
	public int getTotalRequirement() {

	}
}

public class RFP {

	public static void main(String[] args) {
        
		// declare the necessary variables

		// create new object from class Result

		// declare a Scanner object to read input

		// read input and process them accordingly
	}
}