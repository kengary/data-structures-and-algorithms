import java.util.*;

class Result {
	private Vector<String> v;
	
	public Result() {
		v = new Vector<String>();
	}
	
	public void addR(String a) {
		v.add(a);
	}
	
	public String bestProposal(Vector <Proposal> proposal) {
		int best = 0, bestPrice = 1000000000, bestIndex = 0;
		for (int i = 0; i < proposal.size(); i++) {
			int cnt = 0;
			for (int j = 0; j < proposal.get(i).getTotalRequirement(); j++) {
				if (v.contains(proposal.get(i).getRequirement(j))) {
					cnt++;
				}
			}
			int price = proposal.get(i).getPrice();
			if (cnt > best) {
				bestIndex = i;
				best = cnt;
				bestPrice = price;
			}
			else if (cnt == best && price < bestPrice) {
				bestIndex = i;
				best = cnt;
				bestPrice = price;
			}
		}
		return proposal.get(bestIndex).getName();
	}
}

class Proposal {
	private String name;
	private int price, totalRequirement;
	private Vector <String> requirement;
	
	Proposal(String name, int price) {
		this.name = name;
		this.price = price;
		requirement = new Vector <String>();
	}
	
	public int getPrice() {
		return price;
	}
	
	public String getName() {
		return name;
	}
	
	public void addRequirement(String name) {
		requirement.add(name);
		totalRequirement++;
	}
	
	public String getRequirement(int index) {
		return requirement.get(index);
	}
	
	public int getTotalRequirement() {
		return totalRequirement;
	}
}

public class RFP {

	public static void main(String[] args) {
		Result r = new Result();
		Scanner sc = new Scanner(System.in);
		int tc = 0;
		while (sc.hasNext()) {
			int N, P;
			N = sc.nextInt();
			P = sc.nextInt();
			for (int i = 0; i < N; i++) r.addR(sc.next());
			Vector <Proposal> proposals = new Vector <Proposal>();
			for (int i = 0; i < P; i++) {
				String name = sc.next();
				int price = sc.nextInt();
				int total = sc.nextInt();
				proposals.add(new Proposal(name, price));
				for (int j = 0; j < total; j++) {
					proposals.get(i).addRequirement(sc.next());
				}
			}
			System.out.println("RFP #" + ++tc);
			System.out.println(r.bestProposal(proposals));
			System.out.println();
		}
	}
}
