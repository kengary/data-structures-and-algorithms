import java.util.*;
/*
 * author		: [How Wei Keng]
 * matric no.	: [A0087836M]
 */
class Result {
    // declare the member field
    protected int _numOfRequirement,_numOfProposal;
    protected List <Proposal> _proposal;
    protected List <String> _requirement;

    // declare the constructor
    public Result(int numOfRequirement,int numOfProposal){
        _proposal=new ArrayList<Proposal>(numOfProposal);
        _requirement=new ArrayList<String>();
    }

    // declare accessor method to return the member field

    /* add the requirements given
     * 		PRE-condition:
     *	 	POST-condition:
     */
    public void addRequirement(String requirement) {
        _requirement.add(requirement);
    }

    public void addProposal(Proposal p){
        _proposal.add(p);
    }

    public List<String> getRequirement(){
        return _requirement;
    }

    /* determine which proposals has the best criteria.
       by weightage system:score=(compliace*5 + 1/price + 1/indexPriority)
     * 		PRE-condition:
     *		POST-condition:return name
     */

    public String bestProposal() {
        Proposal bestProposal=_proposal.get(0);
        Proposal tempProposal=new Proposal();
        Iterator <Proposal> iter=_proposal.iterator();

        while(iter.hasNext()){
            tempProposal=iter.next();
            //System.out.println(tempProposal.getName()+" : "+
            //tempProposal.getTotalRequirement());
            if(tempProposal.getTotalRequirement()==bestProposal.getTotalRequirement()){
                if(tempProposal.getPrice()==bestProposal.getPrice()){
                    if(_proposal.indexOf(tempProposal)<_proposal.indexOf(bestProposal)){
                        bestProposal=tempProposal;
                    }
                }else if(tempProposal.getPrice()<bestProposal.getPrice()){
                    bestProposal=tempProposal;
                }
            }else if(tempProposal.getTotalRequirement()>bestProposal.getTotalRequirement()){
                bestProposal=tempProposal;
            }
        }
        return bestProposal.getName();
    }
}

class Proposal {

    // declare the member field
    protected int _price,_numOfRequirement;
    protected List<String> _requirement;
    protected String _name;

    public Proposal(){
        // _requirement=new Arraylist<String>(1);
    }

    // declare the constructor
    public Proposal(String name,int price,int numOfRequirement){
        _name=name;
        _price=price;
        _requirement=new ArrayList <String> ();
    }

    // declare accessor method to return the member field

    /* implement getPrice which will return the price of the corresponding proposal
     * 		PRE-condition:
     *		POST-condition:
     */
    public int getPrice() {
        return _price;
    }

    /* implement getName which will return the name of the corresponding proposal
     * 		PRE-condition:
     *		POST-condition:
     */
    public String getName() {
        return _name;
    }

    /* implement addRequirement which will add the requirement of the corresponding proposal
     * 		PRE-condition:
     *		POST-condition:
     */
    public void addRequirement(String
            requirement,List<String>RFP_requirement) {
        if(RFP_requirement.contains(requirement))
            _requirement.add(requirement);        
    }
    public int getCompliance(List<String>requirement){
        int c;
        c= (_requirement.size())/(requirement.size());
        return c;
    }

    /* implement getRequirement which will get the i-th requirement of the corresponding proposal
     * 		PRE-condition:
     *		POST-condition:
     */
    public String getRequirement(int index) {
        return _requirement.get(index);
    }

    /* implement getTotalRequirement which will get the total requirements of the corresponding proposal
     * 		PRE-condition:
     *		POST-condition:
     */
    public int getTotalRequirement(){
        return _requirement.size();    
    }
}

public class RFP{
    protected int _index;
    /* public RFP(int index){
       _index=index;
       }*/
    public static void main(String[] args) {

        // declare the necessary variables
        int testcaseIndex=1,numOfRequirement=0,numOfProposal=0;        

        // declare a Scanner object to read input
        Scanner input=new Scanner(System.in);

        List <Result> testcase=new ArrayList<Result>();

        while(input.hasNext()){

            // create new testcase obj

            //read num of requirments and proposals
            numOfRequirement=input.nextInt();
            numOfProposal=input.nextInt();

            Result RFP=new Result(numOfRequirement,numOfProposal);

            readRfpRequirement(input,numOfRequirement,RFP);
            readProposal(input,numOfProposal,RFP);

            testcase.add(RFP);

        }
        for(int i=0;i<testcase.size();i++){     
            System.out.println("RFP #"+(i+1));
            //System.out.println("I now is "+i);
            System.out.println(testcase.get((i)).bestProposal()+"\n");
            /*if(i<(testcase.size()-1)){
              System.out.print("\n");
              }*/
        }
    }
    public static void readRfpRequirement(Scanner input,int numOfRequirement,Result RFP){
        for(int i=0;i<numOfRequirement;i++){
            RFP.addRequirement(input.next());
        }
    }

    public static void readProposal(Scanner input,int numOfProposal,Result RFP){
        for(int i=0;i<numOfProposal;i++){
            String name=input.next();
            int price=input.nextInt();
            int numOfR=input.nextInt();
            Proposal p=new Proposal(name,price,numOfR);
            for(int j=0;j<numOfR;j++){
                p.addRequirement(input.next(),RFP.getRequirement());            
            }
            RFP.addProposal(p);
        }
    }

}






