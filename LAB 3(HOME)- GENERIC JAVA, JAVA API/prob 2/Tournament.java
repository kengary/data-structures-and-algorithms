
import java.util.*;
/*
 * author	: [How Wei Keng]
 * matric no.	: [A0087836M]
 */

class Person implements Comparable<Person> {
    // declare the member field

    protected int _points, _seatNum, _cardNum;
    protected String _name;

    // declare the constructor
    public Person(String name, int seatNum, int points) {
        _name = name;
        _seatNum = seatNum;
        _cardNum = seatNum;
        _points = points;
    }
    // declare accessor method to return the member field

    public String getName() {
        return _name;
    }

    public int getSeatNum() {
        return _seatNum;
    }

    public int getCardNum() {
        return _cardNum;
    }

    public int getPoints() {
        return _points;
    }
    // Setters method to set new value

    /*
Purpose: to set points after each iteration
Pre:
Post:
     */
    public void setPoints(int points1, int points2) {
        _points += (points1 + points2);

    }

    /*Purpose: to set new Seat and return the seat num after each
     * iteration
Pre:
Post:Store the new Seat Num to _seatNum
     */
    public void setSeatNum(int randomN, int numOfPerson) {
        int newNumX = 0;
        if ((_seatNum * randomN) >= numOfPerson) {
            newNumX = (_seatNum * randomN) % numOfPerson;
        } else {
            newNumX = _seatNum * randomN;
        }
        _seatNum = newNumX;

    }

    public int compareTo(Person p) {
        return this.getSeatNum() - p.getSeatNum();
    }
    /* getNewSeat: get new seat based on the rule
     * 		PRE-condition:
     *		POST-condition:

     public int getNewSeat(){

     }
     */
    // Add other methods if necessary
}

//Driver class
class Tournament {

    public static void main(String[] args) {
        // declare the necessary variables
        int numOfPerson = 0, randomN = 0;


        // declare a Scanner object to read input
        Scanner scan = new Scanner(System.in);
        numOfPerson = scan.nextInt();

        // read input and create matrix to stimulate seating arrangement
        //PRE: Check M > 3
        Person[] personMatrix;
        if (numOfPerson < 4) {
            System.exit(0);
        } else {
            personMatrix = new Person[numOfPerson];
            randomN = scan.nextInt();
            createPersonMatrix(scan, personMatrix, numOfPerson);


            // process the rearragment with points
            processArrangement(randomN, numOfPerson, personMatrix);
            printHighest(numOfPerson, personMatrix);
            System.exit(0);
            //print the output
        }
    }

    public static void createPersonMatrix(Scanner scan, Person[] personMatrix, int numOfPerson) {
        for (int i = 0; i < numOfPerson; i++) {
            Person p = new Person(scan.next(), scan.nextInt(), 0);
            personMatrix[i] = p;
        }
    }

    public static void processArrangement(int randomN, int numOfPerson, Person[] personMatrix) {
        for (int i = 0; i < randomN; i++) {

            for (int matrixIndex = 0; matrixIndex < numOfPerson; matrixIndex++) {
                personMatrix[matrixIndex].setSeatNum(randomN, numOfPerson);
                int newSeatNum = personMatrix[matrixIndex].getSeatNum();
            }
            processScore(personMatrix, numOfPerson);
        }
    }

    public static void processScore(Person[] personMatrix, int numOfPerson) {
        Arrays.sort(personMatrix);
        for (int matrixIndex = 0; matrixIndex < numOfPerson; matrixIndex++) {
            int newSeatNum = personMatrix[matrixIndex].getSeatNum();
            if (newSeatNum == 0) {
                personMatrix[matrixIndex].setPoints(personMatrix[(numOfPerson - 1)].getCardNum(), personMatrix[((newSeatNum + 1) % numOfPerson)].getCardNum());
            } else {
                personMatrix[matrixIndex].setPoints(personMatrix[(newSeatNum - 1) % numOfPerson].getCardNum(), personMatrix[((newSeatNum + 1)) % numOfPerson].getCardNum());

            }
        }
    }

    public static void printHighest(int numOfPerson, Person[] personMatrix) {

        String highestName = "ABCD";
        int highestScore = 0;
        for (int i = 0; i < numOfPerson; i++) {
            if (personMatrix[i].getPoints() > highestScore) {
                highestScore = personMatrix[i].getPoints();
                highestName = personMatrix[i].getName();
            }
        }
        System.out.println(highestName + " " + highestScore);
    }
}
