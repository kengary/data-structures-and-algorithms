import java.util.*;
/*
 * author		: [How Wei Keng]
 * matric no.	: [A0087836M]
 */
class Person {
	// declare the member field

	// declare the constructor

	// declare accessor method to return the member field

	/* getNewSeat: get new seat based on the rule
	 * 		PRE-condition:
	 *		POST-condition:
	 */
    public int getNewSeat(){
    }

    // Add other methods if necessary
}


//Driver class
class Tournament{

    public static void main(String[] args){
		// declare the necessary variables

		// declare a Scanner object to read input

		// read input and process them accordingly
    }
}