import java.util.*;
class Matrix {      
	private int _row, _col;
	private int [][] _myMatrix;

    // Constructor
	public Matrix (int row, int col) {
		_row = row;
		_col = col;
		_myMatrix = new int [row][col];
	}

	public  int getRow() {
		return _row;
	}

	public int getCol() {
		return _col;
	}

	public int getEntry(int row, int col) {
        return _myMatrix[row][col];
	}

	public void setEntry(int row, int col, int value) {
        _myMatrix[row][col] = value;
	}

	//precond: the two matrix must have the same dimemsions
	//postcond: the result of addition is stored in the original matrix
	//purpose: Adding two matrices
	public void addMatrix(Matrix thatMatrix) {
		for (int rIndex = 0; rIndex < _row; rIndex++)
			for (int cIndex = 0; cIndex < _col; cIndex++) {
				_myMatrix[rIndex][cIndex] += thatMatrix.getEntry(rIndex,cIndex);
			}

	}

	//purpose: to multiply two matrices
    //precond: the number of rows of the first matrix must be
	//         the same as the number of columns of the second matrix.
	//postcond: the result of the multiplication is stored in the original matrix
	public void multiMatrix(Matrix thatMatrix) {
		int sum;
		int row = _row;
		int col = thatMatrix.getCol();
		int [][] result = new int [row][col]; 
		for (int rIndex = 0; rIndex < row; rIndex++) 
			for (int cIndex = 0; cIndex < col; cIndex++) {
			    sum = 0;
			    for (int pIndex = 0; pIndex < _col; pIndex++)
			        sum = sum + _myMatrix[rIndex][pIndex] * thatMatrix.getEntry(pIndex,cIndex);
				result[rIndex][cIndex]=sum;
			}
        _myMatrix = result;
	}
}

    // Driver class to test the matrix operations
	public class TestMatrix1 {

		public static void main (String [] args) {

			Scanner input = new Scanner(System.in);
			int row1, col1, row2, col2;

			// read in the dimensions
			row1 = input.nextInt();
			col1 = input.nextInt();
			row2 = input.nextInt();
			col2 = input.nextInt();

			// create the two matrices
			Matrix fMatrix = new Matrix(row1,col1);
			Matrix sMatrix = new Matrix(row2,col2);

			// read in the values in the two matrices
	
			readMatrix(input,fMatrix,row1,col1);
			readMatrix(input,sMatrix,row2,col2);

			// call the method to perform the operation
			fMatrix.multiMatrix(sMatrix);

			// print the resultant matrix

			printMatrix(fMatrix, row1, col1);

		}

		// Purpose: to read in the matrix from the input
		// precond: Scanner and Matrix objects have been created
		//          and dimension of Matrix object is known
		// Postcond: the Matrix object is filled with values
		public static void readMatrix(Scanner input, Matrix matrix, int row, int col) {
			for (int rIndex = 0; rIndex < row; rIndex++)
			    for (int cIndex = 0; cIndex < col; cIndex++) 
			        matrix.setEntry(rIndex, cIndex, input.nextInt());
		}

		// Purpose: to print the matrix
		// precond: Dimension of matrix is known
		// postcond: the matrix is printed
		public static void printMatrix(Matrix matrix, int row, int col) {
			for (int rIndex = 0; rIndex < row; rIndex++) {
			    System.out.print(matrix.getEntry(rIndex, 0));
			    for (int cIndex = 1; cIndex < col - 1; cIndex++) 
			    	System.out.print(" " + matrix.getEntry(rIndex, cIndex));
				System.out.println();
			}
        }


}
