/*
 * author       : [How Wei Keng]
 * matric no.  : [A0087836]
 */

import java.util.*;

class Matrix{

    //declare var
    int _row,_col;
    int [][] _myMatrix;

    //Constructor
    public Matrix(int N){
        _row=N;
        _col=N;
        _myMatrix=new int[N][N];
    }

    //getters
    //get the matrix value
    public int getEntry(int row,int col){
        return _myMatrix[row][col];
    }

    //setters
    //set value to specified matrix index
    public void setEntry(int row,int col,int value){
        _myMatrix[row][col]=value;
    }

    //mutators

    //algo= N-(row index +1)
    //PRE:-
    //POST: store the result matrix to _myMatrix
    //Purpose to reflect the matrix to x-axis

    public void reflectX(){
        int [][] cloneMatrix=new int[_row][_col];

outer:
        for(int cIndex=0;cIndex<_col;cIndex++){
inner:
            for(int rIndex=0;rIndex<_row;rIndex++){
                cloneMatrix[rIndex][cIndex]=this.getEntry((_row-(rIndex+1)),cIndex);
            }//end inner        
        }//end outer
        _myMatrix=cloneMatrix;
    }

    //PRE:-
    //POST: store the result matrix to _myMatrix
    //Purpose to reflect the matrix to y-axis
    public void reflectY(){
        int [][] cloneMatrix=new int[_row][_col];
outer:
        for(int rIndex=0;rIndex<_row;rIndex++){
inner:
            for(int cIndex=0;cIndex<_col;cIndex++){
                cloneMatrix[rIndex][cIndex]=this.getEntry(rIndex,(_col-(cIndex+1)));
            }//inner   
        }//outer
        _myMatrix=cloneMatrix;
    }

    //Pre:  degree=90|180|270
    //POST: store the result matrix to _myMatrix
    //Purpose: Rotate the matrix in specified degree
    public void rotate(int degree){
        if(degree==180){
            this.reflectY();
            this.reflectX();
        }else if(degree==90){

            int [][] cloneMatrix=new int[_row][_col];

outer:
            for(int rIndex=0;rIndex<_row;rIndex++){
inner:
                for(int cIndex=0;cIndex<_col;cIndex++){
                    cloneMatrix[rIndex][cIndex]=this.getEntry((_row-(cIndex+1)),rIndex);
                }//end inner        
            }//end outer
            _myMatrix=cloneMatrix;

        }else if(degree==270){
            this.rotate(180);
            this.rotate(90);
        }else{
            System.exit(0);
        }
    }

    //Run point
    //Check the input type and do the operation
    public void operate(String operation,String type){
        if(operation.equals("Reflect") && type.equals("y")){
            this.reflectY();
        }else if(operation.equals("Reflect")&& type.equals("x")){
            this.reflectX();
        }else if(operation.equals("Rotate")){
            try{
                this.rotate(Integer.parseInt(type));
            }catch(Exception ex){
                System.exit(0);
            }
        }else{
            System.exit(0);
        }
    }
}
//Driver class
public class Transformation{
    public static void main(String []args){
        //declare var
        int N,row,col;

        //Scanner obj
        Scanner input=new Scanner(System.in);        
        //read and process input
        N=input.nextInt();
        Matrix m1=new Matrix(N);
        readMatrix(input,m1,N);
        //stimulate problem
        readProcessOps(input,m1,N);
        //print output
        printMatrix(m1,N);
    }
    //Pre:N>=1 && N<=100
    public static void readMatrix(Scanner input,Matrix matrix,int N){
        if(N>100 || N <1){
            System.exit(0);
        }else{
            for(int rIndex=0;rIndex<N;rIndex++){
                for(int cIndex=0;cIndex<N;cIndex++){
                    matrix.setEntry(rIndex,cIndex,input.nextInt());
                }
            }
        }
    }
    //Pre: ops !(input<1 || >100)
    public static void readProcessOps(Scanner input,Matrix matrix,int N){
        int numOps;
        try{

            numOps=input.nextInt();
            if(numOps<1||numOps>100){
                System.exit(0);
            }else{
                for(int ops=0;ops<numOps;ops++){
                    matrix.operate(input.next(),input.next());
                }
            }
        }catch(Exception ex){System.exit(0);}

    }
    public static void printMatrix(Matrix matrix,int N){
        for(int rIndex=0;rIndex<N;rIndex++){
            for(int cIndex=0;cIndex<N;cIndex++){
                if(cIndex!=(N-1)){
                    System.out.print(matrix.getEntry(rIndex,cIndex)+" ");      
                }else{
                    System.out.print(matrix.getEntry(rIndex,cIndex));
                }}
            System.out.println();
        }
    }
}
